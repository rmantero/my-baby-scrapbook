//
//  TextViewModel.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "TextViewModel.h"


@implementation TextViewModel


// Specify default values for properties
+ (NSDictionary *)defaultPropertyValues
{
    return
        @{
            @"textViewId" : @"",
            @"textViewText" : @""
        };
}


// Specify primary key
+ (NSString *)primaryKey
{
    return @"textViewId";
}


// Specify properties to ignore (Realm won't persist these)

+ (NSArray *)ignoredProperties
{
    return @[];
}


@end
