//
//  PageController.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface PageController : UIPageViewController


@property (assign, nonatomic) int currentIndex;
@property (strong, nonatomic) UIPageViewController *pageViewController;


- (IBAction)menu:(id)sender;
- (void)closeMenu;
- (void)setPage:(int)index animated:(BOOL)animated;



@end