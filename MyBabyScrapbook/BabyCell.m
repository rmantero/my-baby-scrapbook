//
//  BabyCell.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 21/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "BabyCell.h"


@implementation BabyCell


- (void)awakeFromNib
{
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    _imageViewPhoto.image = nil;
    _labelName.text = @"";
    _imageViewPhoto.layer.borderColor = [UIColor clearColor].CGColor;
}


- (void)setSelected:(BOOL)selected
{
    if (selected)
    {
        _imageViewPhoto.layer.borderColor = self.isFemale ? [kColor_PinkDark CGColor] : [kColor_CyanDark CGColor];
        _imageViewPhoto.layer.borderWidth = 7.0f;
    }
    else
    {
        _imageViewPhoto.layer.borderColor = self.isFemale ? [kColor_Pink CGColor] : [kColor_Cyan CGColor];

        _imageViewPhoto.layer.borderWidth = 6.0f;
    }
}


@end
