//
//  BabyModel.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"


@interface BabyModel : RLMObject


@property NSString *babyId;
@property NSString *babyName;
@property NSData *babyPhoto;
@property BOOL babyIsFemale;
@property BOOL babyHasPhoto;
@property int babyCurrentPage;


@end