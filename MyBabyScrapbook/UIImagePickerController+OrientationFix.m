//
//  UIImagePickerController+OrientationFix.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 13/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "UIImagePickerController+OrientationFix.h"


@interface UIImagePickerController_OrientationFix ()


@end


@implementation UIImagePickerController (OrientationFix)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}


- (BOOL)shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


@end
