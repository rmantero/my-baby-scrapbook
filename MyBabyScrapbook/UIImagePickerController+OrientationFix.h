//
//  UIImagePickerController+OrientationFix.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 13/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import <UIKit/UIKit.h>


//!!!!:This is part of forcing photolibrary to landscape against Apple's rules. Delete if rejected
@interface UIImagePickerController_OrientationFix : UIImagePickerController


@end
