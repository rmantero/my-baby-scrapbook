//
//  BabySelector.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "BabySelector.h"
#import "BabyCell.h"
#import "BabyEditor.h"
#import "PageController.h"
#import "AppDelegate.h"


@interface BabySelector () <DrapDropCollectionViewDelegate, UICollectionViewDataSource>
{
    BOOL drop;
}


@property (weak, nonatomic) IBOutlet DragDropCollectionView *theCollectionview;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (weak, nonatomic) IBOutlet UIButton *buttonStart;


@end


#pragma mark - Initialization
#pragma mark -


@implementation BabySelector


- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([defaults objectForKey:@"currentBaby"] && [[defaults objectForKey:@"currentBaby"] length] > 0 && alreadyInitialized == NO)
    {
        [self start:nil];

        return;
    }

    _labelName.text = @"";
    _buttonEdit.hidden = YES;
    _buttonStart.alpha = 0.3;
    _buttonStart.enabled = NO;

    _theCollectionview.allowsMultipleSelection = NO;

    //Dragging delegate
    _theCollectionview.draggingDelegate = self;
    [_theCollectionview enableDragging:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Check if we already have a member list
    RLMResults *results = [MemberModel allObjects];
    if (results)
    {
        if (results.count > 0)
        {
            _memberModel = results.firstObject;

            //Set baby name
            _selectedBaby = [defaults objectForKey:@"currentBaby"];
            if (_selectedBaby.length > 0)
            {
                BabyModel *current = [BabyModel objectForPrimaryKey:_selectedBaby];
                _labelName.text = current.babyName;

                _buttonStart.alpha = 1.0;
                _buttonStart.enabled = YES;
            }

            [_theCollectionview reloadData];

            return;
        }
    }

    //Create member list
    _memberModel = [[MemberModel alloc] init];
    [_memberModel.bookMembers removeAllObjects];
    [defaultRealm beginWriteTransaction];
    [defaultRealm addObject:_memberModel];
    [defaultRealm commitWriteTransaction];
}


#pragma mark - System
#pragma mark -


/**
 *  Set status bar color
 *
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


/**
 *  Hides status bar in iOS7
 *
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/**
 *  Define allowed orientations
 *
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


/**
 *  Enable/Disable autorotation
 *
 */
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - End
#pragma mark -


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toBabyEditorSegue"])
    {
        BabyEditor *editor = (BabyEditor *)segue.destinationViewController;
        editor.memberId = _selectedBaby;
        editor.parentVC = self;
    }

    if ([segue.identifier isEqualToString:@"toNewBabyEditorSegue"])
    {
        BabyEditor *editor = (BabyEditor *)segue.destinationViewController;
        editor.memberId = nil;
        editor.parentVC = self;
    }
}


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


#pragma mark - Actions
#pragma mark -


- (IBAction)start:(id)sender
{
    if ([defaults objectForKey:@"currentBaby"] && [[defaults objectForKey:@"currentBaby"] length] > 0)
    {
        //Select/Create Realm for this baby
        realm = [RLMRealm realmWithPath:[[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.realm", [defaults objectForKey:@"currentBaby"]]]];

        //Go to book
        PageController *pcont = (PageController *)[storyboard instantiateViewControllerWithIdentifier:@"PageController"];
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [delegate changeRootViewController:pcont];
    }
}


- (IBAction)edit:(id)sender
{
    //Set blur background
    if (![self.view.subviews containsObject:blurView])
    {
        blurView.frame = self.view.frame;
        [self.view addSubview:blurView];
    }

    [self performSegueWithIdentifier:@"toBabyEditorSegue" sender:self];
}


#pragma mark - Methods
#pragma mark -


- (void)updateView
{
    _selectedBaby = [defaults objectForKey:@"currentBaby"];
    if (_selectedBaby.length > 0)
    {
        BabyModel *current = [BabyModel objectForPrimaryKey:_selectedBaby];
        _labelName.text = current.babyName;

        _buttonStart.alpha = 1.0;
        _buttonStart.enabled = YES;
    }
    else
    {
        _labelName.text = @"";

        _buttonStart.alpha = 0.3;
        _buttonStart.enabled = NO;
    }

    [_theCollectionview reloadData];
}


#pragma mark - UICollectionViewDelegate
#pragma mark -


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // Always one extra cell for the "add" cell
    if (_memberModel.bookMembers.count > 0)
    {
        return _memberModel.bookMembers.count + 1;
    }

    return 1;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)theCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.item == _memberModel.bookMembers.count) //Add cell
    {
        BabyCell *cell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"AddNewCell" forIndexPath:indexPath];

        cell.labelName.text = @""; //NSLocalizedString(@"babySelector_addNew", nil);

        //Set flag to avoid dragging over this cell
        cell.tag = -23;

        return cell;
    }
    else //Baby cell
    {
        BabyCell *cell = [theCollectionView dequeueReusableCellWithReuseIdentifier:@"BabyCell" forIndexPath:indexPath];

        BabyModel *b = _memberModel.bookMembers[indexPath.item];

        //Rounded frame
        cell.imageViewPhoto.layer.cornerRadius = cell.imageViewPhoto.frame.size.width / 2;
        cell.imageViewPhoto.layer.borderColor = b.babyIsFemale ? [kColor_Pink CGColor] : [kColor_Cyan CGColor];
        cell.imageViewPhoto.layer.borderWidth = 6.0f;
        [cell.imageViewPhoto.layer setMasksToBounds:YES];

        //Set gender
        cell.isFemale = b.babyIsFemale;

        // Set photo
        if (b.babyPhoto == nil) //No photo
        {
            NSString *gender = @"defaultAvatarMale";
            if (b.babyIsFemale)
            {
                gender = @"defaultAvatarFemale";
            }

            cell.imageViewPhoto.image = [UIImage imageNamed:gender];
            cell.imageViewPhoto.contentMode = UIViewContentModeScaleAspectFit;
        }
        else
        {
            cell.imageViewPhoto.image = [UIImage imageWithData:b.babyPhoto];
            cell.imageViewPhoto.contentMode = UIViewContentModeScaleAspectFill;
        }

        // Set name
        cell.labelName.text = b.babyName;
        cell.labelName.textColor = b.babyIsFemale ? kColor_PinkDark : kColor_CyanDark;

        //Select current baby cell
        if ([b.babyId isEqualToString:_selectedBaby])
        {
            [cell setSelected:YES];
        }

        //Set flag to allow dragging
        cell.tag = 5;

        return cell;
    }

    return nil;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!drop)
    {
        if (indexPath.item == _memberModel.bookMembers.count)
        {
            //Set blur background
            if (![self.view.subviews containsObject:blurView])
            {
                blurView.frame = self.view.frame;
                [self.view addSubview:blurView];
            }

            // Adding new member
            [self performSegueWithIdentifier:@"toNewBabyEditorSegue" sender:self];
        }
        else
        {
            // Selecting existing member
            _selectedBaby = _memberModel.bookMembers[indexPath.item][@"babyId"];
            _labelName.text = _memberModel.bookMembers[indexPath.item][@"babyName"];

            [defaults setObject:_selectedBaby forKey:@"currentBaby"];
            [defaults synchronize];

            _buttonEdit.hidden = NO;
            _buttonStart.alpha = 1.0;
            _buttonStart.enabled = YES;

            [_theCollectionview reloadData];
        }
    }
    else //This selection was only a drop
    {
        //Reset flag
        drop = NO;
    }
}


#pragma mark - DrapDropCollectionViewDelegate
#pragma mark -


- (void)dragDropCollectionViewDraggingDidBeginWithCellAtIndexPath:(NSIndexPath *)indexPath
{
    drop = YES;

    [_theCollectionview startWiggle];
}


- (void)dragDropCollectionViewDidMoveCellFromInitialIndexPath:(NSIndexPath *)initialIndexPath toNewIndexPath:(NSIndexPath *)newIndexPath
{
    Log(@"Member moved from index %d to index %d", initialIndexPath.row, newIndexPath.row);

    [defaultRealm beginWriteTransaction];
    BabyModel *b = _memberModel.bookMembers[initialIndexPath.item];
    [_memberModel.bookMembers removeObjectAtIndex:initialIndexPath.item];
    [_memberModel.bookMembers insertObject:b atIndex:newIndexPath.item];
    [defaultRealm commitWriteTransaction];
}


- (void)dragDropCollectionViewDraggingDidEndForCellAtIndexPath:(NSIndexPath *)indexPath
{
    [_theCollectionview stopWiggle];

    //Delay to allow stopWiggle animation to end
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(),
        ^{
            [_theCollectionview reloadData];
        });
}


@end
