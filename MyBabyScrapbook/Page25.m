//
//  Page25.m
//  MyBabyScrapbook
//
//  Created by Richie on 20/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page25.h"
#import "Globals.h"


@interface Page25 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page25


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


@end
