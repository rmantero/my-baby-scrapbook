//
//  MemberModel.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "BabyModel.h"
#import "Globals.h"


RLM_ARRAY_TYPE(BabyModel)


@interface MemberModel : RLMObject


@property RLMArray<BabyModel> *bookMembers;


@end