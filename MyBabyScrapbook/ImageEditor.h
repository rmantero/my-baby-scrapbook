//
//  ImageEditor.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "HFImageEditorViewController.h"
#import "HFImageEditorFrameView.h"


@interface ImageEditor : HFImageEditorViewController


@property (weak, nonatomic) IBOutlet HFImageEditorFrameView *frameView;


@end
