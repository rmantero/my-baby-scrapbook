//
//  PhotoModel.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "PhotoModel.h"


@implementation PhotoModel


// Specify default values for properties
+ (NSDictionary *)defaultPropertyValues
{
    return
        @{
            @"photoId" : @"",
            @"photoData" : @""
        };
}


// Specify primary key
+ (NSString *)primaryKey
{
    return @"photoId";
}


// Specify properties to ignore (Realm won't persist these)

+ (NSArray *)ignoredProperties
{
    return @[];
}


@end
