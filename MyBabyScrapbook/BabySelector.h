//
//  BabySelector.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"


@interface BabySelector : UIViewController


@property (strong, nonatomic) MemberModel *memberModel;
@property (strong, nonatomic) NSString *selectedBaby;


- (void)updateView;


@end