//
//  MemberModel.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "MemberModel.h"


@implementation MemberModel


// Specify default values for properties
+ (NSDictionary *)defaultPropertyValues
{
    return
        @{};
}


// Specify properties to ignore (Realm won't persist these)

+ (NSArray *)ignoredProperties
{
    return @[];
}


@end
