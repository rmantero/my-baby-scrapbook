//
//  Helper.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 9/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Helper.h"
#import "Globals.h"


@implementation Helper


/**
 *  Add color overlay to image
 *
 *  @param image Image to tint
 *  @param color Color to apply
 *
 *  @return Tinted image
 */
+ (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color
{
    UIGraphicsBeginImageContext(image.size);

    CGRect contextRect;
    contextRect.origin.x = 0.0f;
    contextRect.origin.y = 0.0f;
    contextRect.size = [image size];
    // Retrieve source image and begin image context
    CGSize itemImageSize = [image size];
    CGPoint itemImagePosition;
    itemImagePosition.x = ceilf((contextRect.size.width - itemImageSize.width) / 2);
    itemImagePosition.y = ceilf((contextRect.size.height - itemImageSize.height));

    UIGraphicsBeginImageContext(contextRect.size);

    CGContextRef c = UIGraphicsGetCurrentContext();
    // Setup shadow
    // Setup transparency layer and clip to mask
    CGContextBeginTransparencyLayer(c, NULL);
    CGContextScaleCTM(c, 1.0, -1.0);
    CGContextClipToMask(c, CGRectMake(itemImagePosition.x, -itemImagePosition.y, itemImageSize.width, -itemImageSize.height), [image CGImage]);
// Fill and end the transparency layer
#ifdef __x86_64__
    const double *colors = CGColorGetComponents(color.CGColor);
#else
    const float *colors = CGColorGetComponents(color.CGColor);
#endif
    CGContextSetRGBFillColor(c, colors[0], colors[1], colors[2], .75);

    contextRect.size.height = -contextRect.size.height;
    contextRect.size.height -= 15;
    CGContextFillRect(c, contextRect);
    CGContextEndTransparencyLayer(c);

    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}


/**
 *  Add round corners and a subtle border to views
 *  Used for textviews containers
 *
 *  @param object An UIView
 */
+ (void)roundCornersToTextView:(UIView *)object
{
    object.layer.borderWidth = 1.0f;
    object.layer.borderColor = kColor_DarkOverlay.CGColor;
    [Helper roundCornersWithRadius:10.0f toView:object];
}


/**
 *  Add round corners to any UIView
 *
 *  @param radius Radius
 *  @param object View to round corners
 */
+ (void)roundCornersWithRadius:(CGFloat)radius toView:(UIView *)object
{
    object.layer.cornerRadius = radius;
    object.layer.masksToBounds = YES;
}


/**
 *  Enable or disable page controller scroll
 *
 *  @param enabled Should scroll?
 */
+ (void)pagingEnabled:(BOOL)enabled
{
    for (UIView *view in [[pageController.pageViewController view] subviews])
    {
        if ([view isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)view setScrollEnabled:enabled];
        }
    }
}


/**
 *  Generate a string from current date to the second so it can be used as unique ID
 *
 *  @return A sort of unique numeric string
 */
+ (NSString *)uniqueIdFromCurrentDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [df setTimeZone:gmt];
    [df setDateFormat:@"yyyyMMddHHmmss"];
    return [df stringFromDate:[NSDate date]];
}


+ (UIImage*) compositeImage:(UIImage*) sourceImage onPath:(UIBezierPath*) path usingBlendMode:(CGBlendMode) blend;
{
    // Create a new image of the same size as the source.
    UIGraphicsBeginImageContext([sourceImage size]);

    // First draw an opaque path...
    [path fill];
    // ...then composite with the image.
    [sourceImage drawAtPoint:CGPointZero blendMode:blend alpha:1.0];

    // With drawing complete, store the composited image for later use.
    UIImage *maskedImage = UIGraphicsGetImageFromCurrentImageContext();

    // Graphics contexts must be ended manually.
    UIGraphicsEndImageContext();

    return maskedImage;
}


+ (UIImage*) maskImage:(UIImage*) sourceImage toAreaInsidePath:(UIBezierPath*) maskPath;
{
    return [Helper compositeImage:sourceImage onPath:maskPath usingBlendMode:kCGBlendModeSourceIn];
}


+ (UIImage*) maskImage:(UIImage*) sourceImage toAreaOutsidePath:(UIBezierPath*) maskPath;
{
    return [Helper compositeImage:sourceImage onPath:maskPath usingBlendMode:kCGBlendModeSourceOut];
}


@end
