//
//  Helper.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Helper : NSObject


+ (UIImage *)tintImage:(UIImage *)image withColor:(UIColor *)color;
+ (void)roundCornersWithRadius:(CGFloat)radius toView:(UIView *)object;
+ (void)roundCornersToTextView:(UIView *)object;
+ (void)pagingEnabled:(BOOL)enabled;
+ (NSString *)uniqueIdFromCurrentDate;
+ (UIImage*) compositeImage:(UIImage*) sourceImage onPath:(UIBezierPath*) path usingBlendMode:(CGBlendMode) blend;
+ (UIImage*) maskImage:(UIImage*) sourceImage toAreaInsidePath:(UIBezierPath*) maskPath;
+ (UIImage*) maskImage:(UIImage*) sourceImage toAreaOutsidePath:(UIBezierPath*) maskPath;


@end
