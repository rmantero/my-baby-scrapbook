//
//  Page26.m
//  MyBabyScrapbook
//
//  Created by Richie on 20/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page26.h"
#import "Globals.h"


@interface Page26 ()


@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage2;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage3;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage4;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage5;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage6;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage7;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page26


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Random rotation
//    _buttonImage1.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage2.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage3.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage4.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage5.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage6.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage7.superview.superview.tag = arc4random_uniform(16) - 8;

    //Load page assets from Realm
    [self loadItem:_buttonImage1];
    [self loadItem:_buttonImage2];
    [self loadItem:_buttonImage3];
    [self loadItem:_buttonImage4];
    [self loadItem:_buttonImage5];
    [self loadItem:_buttonImage6];
    [self loadItem:_buttonImage7];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


@end
