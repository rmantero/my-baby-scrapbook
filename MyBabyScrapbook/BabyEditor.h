//
//  BabyEditor.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"
#import "BabySelector.h"


@interface BabyEditor : UIViewController


@property (nonatomic, copy) NSString *memberId;
@property (strong, nonatomic) BabySelector *parentVC;


@end