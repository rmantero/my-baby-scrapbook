//
//  AppDelegate.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "AppDelegate.h"
#import "Globals.h"
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()


@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Set user defaults
    defaults = [NSUserDefaults standardUserDefaults];

    //Set storyboard
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    //Set default Realm
    defaultRealm = [RLMRealm defaultRealm];

    //Set var for holding textviews frames before animate
    originalFrame = CGRectZero;

    //Set blur view
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurDarkEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    blurDarkView = [[UIVisualEffectView alloc] initWithEffect:blurDarkEffect];

    //Enable keyboard manager
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [[IQKeyboardManager sharedManager] setShouldToolbarUsesTextFieldTintColor:YES];
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setShouldFixTextViewClip:YES];
    [[IQKeyboardManager sharedManager] setCanAdjustTextView:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:50.0f];

    //Crashlytics initialization
    [Crashlytics startWithAPIKey:kCRASHLYTICS_API_KEY];

    //Set defaults
    if (![defaults objectForKey:@"currentBaby"])
    {
        [defaults setObject:@"" forKey:@"currentBaby"];
    }

    if (![defaults objectForKey:@"firstTooth"])
    {
        [defaults setInteger:0 forKey:@"firstTooth"];
    }

    if (![defaults objectForKey:@"dummy"])
    {
        [defaults setInteger:0 forKey:@"dummy"];
    }

    if (![defaults objectForKey:@"foods"])
    {
        [defaults setObject:@[@0,@0,@0,@0,@0,@0] forKey:@"foods"];
    }

    [defaults synchronize];

    return YES;
}


- (void)changeRootViewController:(UIViewController *)viewController
{
    if (!self.window.rootViewController)
    {
        self.window.rootViewController = viewController;
        return;
    }

    UIView *snapShot = [self.window snapshotViewAfterScreenUpdates:YES];
    [viewController.view addSubview:snapShot];
    self.window.rootViewController = viewController;

    [UIView animateWithDuration:0.3 animations:^{
        snapShot.layer.opacity = 0;
        snapShot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
    }
        completion:^(BOOL finished) {
        [snapShot removeFromSuperview];
        }];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    //Save current page for selected baby
    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];

    [defaultRealm beginWriteTransaction];
    baby.babyCurrentPage = (int)pageController.currentIndex;
    [defaultRealm commitWriteTransaction];
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
