//
//  ImageEditor.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "HFImageEditorViewController+Private.h"
#import "ImageEditor.h"


@interface ImageEditor ()


@end


@implementation ImageEditor


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.cropRect = CGRectMake(0, 0, 500, 500);
        self.minimumScale = 0.2;
        self.maximumScale = 10;
    }
    return self;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}


#pragma mark Hooks
- (void)startTransformHook
{
    //Edit begun
}


- (void)endTransformHook
{
    //Edit ended
}


@end
