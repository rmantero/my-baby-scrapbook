//
//  Globals.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"


@implementation Globals


/**
 *  Global vars
 */
NSUserDefaults *defaults = nil;
UIStoryboard *storyboard = nil;
RLMRealm *realm = nil;
RLMRealm *defaultRealm = nil;
CGRect originalFrame;
CGFloat originalRotation;
PageController *pageController = nil;
UIBlurEffect *blurEffect = nil;
UIVisualEffectView *blurView = nil;
UIBlurEffect *blurDarkEffect = nil;
UIVisualEffectView *blurDarkView = nil;
UITapGestureRecognizer *tap = nil;
UIImage *testImage = nil;
Menu *menu = nil;
BOOL alreadyInitialized = NO;
UIButton *buttonIndex = nil;


@end