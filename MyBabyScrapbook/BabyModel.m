//
//  BabyModel.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "BabyModel.h"


@implementation BabyModel


// Specify default values for properties
+ (NSDictionary *)defaultPropertyValues
{
    return
        @{
            @"babyId" : @"",
            @"babyName" : @"",
            @"babyPhoto" : @"",
            @"babyIsFemale" : @NO,
            @"babyHasPhoto" : @NO,
            @"babyCurrentPage" : @0
        };
}


// Specify primary key
+ (NSString *)primaryKey
{
    return @"babyId";
}


// Specify properties to ignore (Realm won't persist these)

+ (NSArray *)ignoredProperties
{
    return @[];
}


@end
