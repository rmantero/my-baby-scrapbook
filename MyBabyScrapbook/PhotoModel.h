//
//  PhotoModel.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"


@interface PhotoModel : RLMObject


@property NSString *photoId;
@property NSData *photoData;


@end