//
//  Globals.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


//Imports
//#import "PageController.h"
#import <Realm/Realm.h>
#import <MyBabyScrapbook-Swift.h>
#import "IQKeyboardManager.h"
#import "HexColor.h"
#import "SDCTextField.h"
#import "LNNumberpad.h"
#import "THNotesTextView.h"
#import "CircularButton.h"
#import "MemberModel.h"
#import "BabyModel.h"
#import "PhotoModel.h"
#import "TextViewModel.h"
#import "TextFieldModel.h"
#import "PageController.h"
#import "Menu.h"
#import "Page.h"
#import "Helper.h"


@interface Globals : NSObject


/**
 *  Global vars
 */
extern PageController *pageController;
extern NSUserDefaults *defaults;
extern UIStoryboard *storyboard;
extern RLMRealm *realm;
extern RLMRealm *defaultRealm;
extern CGRect originalFrame;
extern CGFloat originalRotation;
extern UIBlurEffect *blurEffect;
extern UIVisualEffectView *blurView;
extern UIBlurEffect *blurDarkEffect;
extern UIVisualEffectView *blurDarkView;
extern UITapGestureRecognizer *tap;
extern UIImage *testImage;
extern Menu *menu;
extern BOOL alreadyInitialized;
extern UIButton *buttonIndex;


/**
 *  Global types
 */


/**
 *  Constants
 */
#define kNumberOfPages 27
#define kTextviewEditViewFrame CGRectMake(30, 30, 964, 276)
#define kMaxResolution 800
#define DegreesToRadians(x) ((x)*M_PI / 180.0)


/**
 *  App colors
 */
#define kColor_BlueBallPen [UIColor colorWithHexString:@"1D2B87" alpha:1.0f]
#define kColor_AlmostWhite [UIColor colorWithHexString:@"FEFEFE" alpha:1.0f]
#define kColor_DarkText [UIColor colorWithHexString:@"2D2D30" alpha:1.0f]
#define kColor_DarkOverlay [UIColor colorWithHexString:@"000000" alpha:0.1f]
#define kColor_Pink [UIColor colorWithHexString:@"FFEBF0" alpha:1.0f]
#define kColor_Cyan [UIColor colorWithHexString:@"E6F3FF" alpha:1.0f]
#define kColor_PinkDark [UIColor colorWithHexString:@"FFCCD9" alpha:1.0f]
#define kColor_CyanDark [UIColor colorWithHexString:@"B2D9FF" alpha:1.0f]
#define kColor_Yellow [UIColor colorWithHexString:@"FFF8D7" alpha:1.0f]
#define kColor_Red [UIColor colorWithHexString:@"FC3C39" alpha:1.0f]
#define kColor_Green [UIColor colorWithHexString:@"55B27A" alpha:1.0f]
#define kColor_Blue [UIColor colorWithHexString:@"0EADD2" alpha:1.0f]


/**
 *  App fonts
 */
#define kButtonFont [UIFont fontWithName:@"DINAlternate-Bold" size:24.0f]


/**
 * Services Keys
 */
#define kCRASHLYTICS_API_KEY @"ca33741731fbaa8d23597ed2a4f9dcdecdaca302"


//
//UTILS
//


/**
 *  Returns the emtpy string if the parameter is nil
 */
#define NON_NIL_STRING(VAR) ((nil == VAR) ? @"" : VAR)


/**
 *  Get JSON key or nil if it's null
 */
#define nilOrJSONObjectForKey(JSON_, KEY_) [[JSON_ objectForKey:KEY_] isKindOfClass:[NSNull class]] ? nil : [JSON_ objectForKey:KEY_]


/**
 *  Get JSON key or "" if it's null
 */
#define emptyOrJSONObjectForKey(JSON_, KEY_) [[JSON_ objectForKey:KEY_] isKindOfClass:[NSNull class]] ? @"" : [JSON_ objectForKey:KEY_]


/**
 *  Get JSON key or -1 if it's null
 */
#define negativeOrJSONObjectForKey(JSON_, KEY_) [[JSON_ objectForKey:KEY_] isKindOfClass:[NSNull class]] ? @-1 : [JSON_ objectForKey:KEY_]


/**
 *  A cleaner Log. Only show Logs when we are in DEBUG mode
 */
#ifdef DEBUG
#define Log(FORMAT, ...) printf("\n%s[%d]: %s\n", __PRETTY_FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define Log(...)
#endif


@end