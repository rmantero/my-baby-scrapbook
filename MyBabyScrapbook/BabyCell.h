//
//  BabyCell.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 21/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Globals.h"


@interface BabyCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPhoto;
@property (assign, nonatomic) BOOL isFemale;


@end
