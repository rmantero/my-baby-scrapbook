//
//  Menu.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Menu.h"
#import "Globals.h"
#import "IndexCell.h"


@interface Menu () <UICollectionViewDelegate, UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UICollectionView *theCollectionview;
@property (weak, nonatomic) IBOutlet UILabel *labelBeta;
@property (weak, nonatomic) IBOutlet UIButton *buttonChange;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Menu


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.layer.shadowOffset = CGSizeMake(0, 1);
    self.view.layer.shadowRadius = 2.0;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOpacity = 0.5;
    self.view.clipsToBounds = NO;

    _buttonChange.layer.cornerRadius = _buttonChange.frame.size.width/2;

    //FIXME: Remove in Production
    _labelBeta.text = [NSString stringWithFormat:@"Beta build: %@", [[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"]];

    [_theCollectionview reloadData];

    [self updateCollection];
}


#pragma mark - System
#pragma mark -


/**
 *  Set status bar color
 *
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


/**
 *  Hides status bar in iOS7
 *
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/**
 *  Define allowed orientations
 *
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


/**
 *  Enable/Disable autorotation
 *
 */
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


#pragma mark - Actions
#pragma mark -


#pragma mark - Methods
#pragma mark -


- (void)updateCollection
{
    NSIndexPath *idx = [NSIndexPath indexPathForItem:pageController.currentIndex inSection:0];
    [_theCollectionview selectItemAtIndexPath:idx animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
}


#pragma mark - UICollectionViewDelegate
#pragma mark -


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return kNumberOfPages;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IndexCell *cell = (IndexCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"indexCell" forIndexPath:indexPath];

    cell.imageviewBackground.image = [UIImage imageNamed:[NSString stringWithFormat:@"thumbnail_page%02d", indexPath.item]];
    NSString *title = [NSString stringWithFormat:@"page_title_%02d", indexPath.item];
    cell.labelName.text = [NSString stringWithFormat:@"%02d - %@", indexPath.item + 1, NSLocalizedString(title, nil)];
    cell.labelName.textColor = cell.isSelected ? kColor_Red : kColor_DarkText;
    cell.imageviewBackground.layer.borderColor = kColor_DarkOverlay.CGColor;
    cell.imageviewBackground.layer.borderWidth = 1.0f;
    cell.layer.masksToBounds = YES;

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [pageController setPage:indexPath.item animated:YES];
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}


@end
