//
//  Menu.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import <UIKit/UIKit.h>
@class Globals;


@interface Menu : UIViewController


@property (nonatomic, assign) BOOL expanded;


- (void)updateCollection;


@end