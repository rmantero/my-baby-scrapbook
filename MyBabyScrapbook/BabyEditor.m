//
//  BabyEditor.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "BabyEditor.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HFImageEditorViewController.h"


@interface BabyEditor () <UIGestureRecognizerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate>
{
}


@property (weak, nonatomic) IBOutlet UIButton *buttonCamera;
@property (weak, nonatomic) IBOutlet UIButton *buttonFemale;
@property (weak, nonatomic) IBOutlet UIButton *buttonMale;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;
@property (weak, nonatomic) IBOutlet UIButton *buttonOK;
@property (weak, nonatomic) IBOutlet UITextField *textfieldName;
@property (nonatomic, strong) BabyModel *baby;
@property (nonatomic, strong) ALAssetsLibrary *library;
@property (nonatomic, strong) HFImageEditorViewController *imageEditor;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UIAlertController *actionSheet;
@property (nonatomic, strong) UIPopoverPresentationController *popPresenter;
@property (nonatomic, assign) BOOL selectingPhoto;


@end


#pragma mark - Initialization
#pragma mark -


@implementation BabyEditor


- (void)viewDidLoad
{
    [super viewDidLoad];

    //Set image editor
    _imageEditor = (HFImageEditorViewController *)[storyboard instantiateViewControllerWithIdentifier:@"imageEditor"];
    _imageEditor.checkBounds = YES;
    _imageEditor.rotateEnabled = YES;
    _imageEditor.scaleEnabled = YES;
    _imageEditor.tapToResetEnabled = YES;

    //What to do when finished edition?
    __weak typeof(self) weakSelf = self;
    self.imageEditor.doneCallback = ^(UIImage *editedImage, BOOL canceled) {

        if(!canceled)
        {
            //Save the cropped image to Realm
            [defaultRealm beginWriteTransaction];
            weakSelf.baby.babyPhoto = UIImagePNGRepresentation(editedImage);
            weakSelf.baby.babyHasPhoto = YES;
            [defaultRealm commitWriteTransaction];

            [weakSelf.buttonCamera setImage:editedImage forState:UIControlStateNormal];

            //Close picker
            [weakSelf.imagePicker dismissViewControllerAnimated:YES completion:^{
                weakSelf.selectingPhoto = NO;
            }];
        }
        else
        {
            //Go to picker root
            [weakSelf.imagePicker popToRootViewControllerAnimated:YES];
            [weakSelf.imagePicker setNavigationBarHidden:NO animated:YES];
        }
    };
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Get current baby or create new one
    if (_memberId != nil)
    {
        _baby = [BabyModel objectInRealm:defaultRealm forPrimaryKey:_memberId];
        _buttonDelete.hidden = NO;
    }
    else
    {
        _baby = [[BabyModel alloc] init];
        _baby.babyId = [Helper uniqueIdFromCurrentDate];
        _baby.babyName = NSLocalizedString(@"babyEditor_myGirl", nil);
        _baby.babyIsFemale = YES;
        _baby.babyPhoto = UIImagePNGRepresentation([UIImage imageNamed:@"defaultAvatarFemale"]);

        _buttonDelete.hidden = YES;
    }

    //Set UI
    _textfieldName.placeholder = NSLocalizedString(@"babyEditor_name", nil);
    _textfieldName.text = _baby.babyName;

    [_buttonFemale setSelected:_baby.babyIsFemale];
    [_buttonMale setSelected:!_baby.babyIsFemale];

    _buttonCamera.layer.cornerRadius = _buttonCamera.frame.size.width / 2;
    _buttonCamera.layer.borderWidth = 6.0f;
    _buttonCamera.layer.borderColor = _baby.babyIsFemale ? kColor_PinkDark.CGColor : kColor_CyanDark.CGColor;
    _buttonCamera.layer.masksToBounds = YES;

    [_buttonCamera setImage:[UIImage imageWithData:_baby.babyPhoto] forState:UIControlStateNormal];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeEditor:)];
    [tap setCancelsTouchesInView:NO];
    tap.delegate = self;
    [self.view.window addGestureRecognizer:tap];
}


#pragma mark - System
#pragma mark -


/**
 *  Set status bar color
 *
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


/**
 *  Hides status bar in iOS7
 *
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/**
 *  Define allowed orientations
 *
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


/**
 *  Enable/Disable autorotation
 *
 */
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    //Dismiss view
    if ([self.view.window.gestureRecognizers containsObject:tap])
    {
        tap.cancelsTouchesInView = YES;
        tap.delegate = nil;
        [self.view.window removeGestureRecognizer:tap];
    }


    [super viewWillDisappear:animated];
}


#pragma mark - Actions
#pragma mark -


- (IBAction)camera:(id)sender
{
    //Show source popup
    [self showPhotoPicker];
}


- (IBAction)female:(id)sender
{
    _buttonFemale.selected = YES;
    _buttonMale.selected = NO;

    //Set gender
    [defaultRealm beginWriteTransaction];
    _baby.babyIsFemale = YES;
    [defaultRealm commitWriteTransaction];

    [self updateGender];
}


- (IBAction)male:(id)sender
{
    _buttonMale.selected = YES;
    _buttonFemale.selected = NO;

    //Set gender
    [defaultRealm beginWriteTransaction];
    _baby.babyIsFemale = NO;
    [defaultRealm commitWriteTransaction];

    [self updateGender];
}


- (IBAction) delete:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"babyEditor_alert_title", nil) message:NSLocalizedString(@"babyEditor_alert_message", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"babyEditor_alert_accept", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                             //Save to Realm
                                                             [defaultRealm beginWriteTransaction];

                                                             //remove from babies array
                                                             for (int xx = 0; xx < _parentVC.memberModel.bookMembers.count; xx++)
                                                             {
                                                                 BabyModel *b = _parentVC.memberModel.bookMembers[xx];
                                                                 if ([b.babyId isEqualToString:_baby.babyId])
                                                                 {
                                                                     [_parentVC.memberModel.bookMembers removeObjectAtIndex:xx];
                                                                 }
                                                             }
                                                             
                                                             [defaultRealm deleteObject:_baby];
                                                             
                                                             [defaultRealm commitWriteTransaction];
                                                             
                                                             //delete baby
                                                             [defaults setObject:@"" forKey:@"currentBaby"];
                                                             [defaults synchronize];
                                                             
                                                             [self dismissView];
                                                     }];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"babyEditor_alert_cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                                 return;
                                                         }];

    [alert addAction:actionOK];
    [alert addAction:actionCancel];

    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)ok:(id)sender
{
    //Save to Realm
    [defaultRealm beginWriteTransaction];

    //Add to babies array
    BOOL found = NO;
    for (int xx = 0; xx < _parentVC.memberModel.bookMembers.count; xx++)
    {
        BabyModel *b = _parentVC.memberModel.bookMembers[xx];
        if ([b.babyId isEqualToString:_baby.babyId])
        {
            found = YES;
        }
    }

    if (found == NO)
    {
        [_parentVC.memberModel.bookMembers addObject:_baby];
    }

    [BabyModel createOrUpdateInDefaultRealmWithValue:_baby];

    [defaultRealm commitWriteTransaction];

    //Set baby
    [defaults setObject:_baby.babyId forKey:@"currentBaby"];
    [defaults synchronize];

    [self dismissView];
}


#pragma mark - Methods
#pragma mark -


- (void)updateGender
{
    //If baby photo is not set, change default image to match gender
    if (_baby.babyHasPhoto == NO)
    {
        NSString *gender = @"defaultAvatarMale";
        if (_baby.babyIsFemale)
        {
            gender = @"defaultAvatarFemale";
        }

        //Update default photo
        [defaultRealm beginWriteTransaction];
        _baby.babyPhoto = UIImagePNGRepresentation([UIImage imageNamed:gender]);
        [defaultRealm commitWriteTransaction];

        [_buttonCamera setImage:[UIImage imageNamed:gender] forState:UIControlStateNormal];
    }

    //Update ring color
    _buttonCamera.layer.borderColor = _baby.babyIsFemale ? kColor_PinkDark.CGColor : kColor_CyanDark.CGColor;

    //If baby name is not set, change default name to match gender
    if ([_baby.babyName isEqualToString:NSLocalizedString(@"babyEditor_myGirl", nil)] || [_baby.babyName isEqualToString:NSLocalizedString(@"babyEditor_myBoy", nil)])
    {
        [defaultRealm beginWriteTransaction];
        _baby.babyName = _baby.babyIsFemale ? NSLocalizedString(@"babyEditor_myGirl", nil) : NSLocalizedString(@"babyEditor_myBoy", nil);
        [defaultRealm commitWriteTransaction];

        _textfieldName.text = _baby.babyName;
    }
}


/**
 *  Close full screen photo preview
 */
- (void)closeEditor:(UITapGestureRecognizer *)sender
{
    if (_selectingPhoto)
    {
        return;
    }

    if (sender.state == UIGestureRecognizerStateEnded)
    {
        UIView *rootView = self.view.window.rootViewController.view;
        CGPoint location = [sender locationInView:rootView];

        if (![self.view pointInside:[self.view convertPoint:location fromView:rootView] withEvent:nil])
        {
            if (_textfieldName.isEditing)
            {
                //Close keyboard
                [self.view endEditing:YES];
            }
            else
            {
                [self dismissView];
            }
        }
    }
}


- (void)dismissView
{
    [_parentVC updateView];

    _actionSheet = nil;
    _imageEditor = nil;
    _imagePicker.delegate = nil;
    _imagePicker = nil;

    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [blurView removeFromSuperview];

                             }];
}


- (void)showPhotoPicker
{
    _actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_title", nil)
                                                       message:nil
                                                preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *actionCamera = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_camera", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {

                                                             [self getPhotoFromCamera];
                                                         }];

    UIAlertAction *actionGallery = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_gallery", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {

                                                              [self getPhotoFromGallery];
                                                          }];

    UIAlertAction *actionDeletePhoto = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                  [self deletePhoto];
                                                              }];

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [_actionSheet addAction:actionCamera];
    }

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        [_actionSheet addAction:actionGallery];
    }

    if (_baby.babyHasPhoto == YES)
    {
        [_actionSheet addAction:actionDeletePhoto];
    }
    
    [_actionSheet setModalPresentationStyle:UIModalPresentationPopover];

    _popPresenter = [_actionSheet popoverPresentationController];
    _popPresenter.sourceView = _buttonCamera;
    _popPresenter.sourceRect = _buttonCamera.bounds;
    _popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;

    [self presentViewController:_actionSheet animated:YES completion:nil];
}


- (void)getPhotoFromCamera
{
    _selectingPhoto = YES;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = (id)self;
        _imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        _imagePicker.showsCameraControls = YES;
        _imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;

        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
}


- (void)getPhotoFromGallery
{
    _selectingPhoto = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        //Set photo picker
        _imagePicker = [[UIImagePickerController alloc] init];
        _imagePicker.delegate = (id)self;
        _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _imagePicker.modalPresentationStyle = UIModalPresentationFullScreen;

        //Present photo picker
        [self presentViewController:_imagePicker animated:YES completion:nil];
    }
}


- (void)deletePhoto
{
    _selectingPhoto = YES;

    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_delete", nil)
                                                                        message:NSLocalizedString(@"photopicker_selector_delete_message", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete_accept", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {

                                                         NSString *gender = @"defaultAvatarMale";
                                                         if (_baby.babyIsFemale)
                                                         {
                                                             gender = @"defaultAvatarFemale";
                                                         }

                                                         //Update default photo
                                                         [defaultRealm beginWriteTransaction];
                                                         _baby.babyPhoto = UIImagePNGRepresentation([UIImage imageNamed:gender]);
                                                         _baby.babyHasPhoto = NO;
                                                         [defaultRealm commitWriteTransaction];
                                                         
                                                         [_buttonCamera setImage:[UIImage imageNamed:gender] forState:UIControlStateNormal];

                                                         _selectingPhoto = NO;
                                                     }];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete_cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                             _selectingPhoto = NO;
                                                         }];

    [errorAlert addAction:actionOK];
    [errorAlert addAction:actionCancel];

    [self presentViewController:errorAlert animated:YES completion:nil];
}


#pragma mark - UIImagePickerControllerDelegate
#pragma mark -


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];

    _library = [[ALAssetsLibrary alloc] init];
    [_library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
        UIImage *preview = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];

        _imageEditor.sourceImage = image;
        _imageEditor.previewImage = preview;
        [_imageEditor reset:NO];

        [picker pushViewController:_imageEditor animated:YES];
        [picker setNavigationBarHidden:YES animated:NO];

    } failureBlock:^(NSError *error) {
        NSLog(@"Failed to get asset from library");
    }];

    //Save photo to camera roll
    //    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    //    {
    //        UIImage* takenImage = info[UIImagePickerControllerOriginalImage];
    //
    //        // Request to save the image to camera roll
    //        [_library writeImageToSavedPhotosAlbum:[takenImage CGImage] orientation:(ALAssetOrientation)[takenImage imageOrientation] completionBlock:^(NSURL* assetURL, NSError* error) {
    //            if (error)
    //            {
    //                Log(@"ERROR: Photo not saved to library: %@",error.localizedDescription);
    //            }
    //        }];
    //    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        _selectingPhoto = NO;
    }];
}


#pragma mark - UIGestureRecognizerDelegate
#pragma mark -


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


#pragma mark - UITextFieldDelegate
#pragma mark -


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [defaultRealm beginWriteTransaction];
    _baby.babyName = textField.text;
    [defaultRealm commitWriteTransaction];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];

    return YES;
}


@end
