//
//  UIViewController+OrientationFix.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 13/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "UIViewController+OrientationFix.h"


@interface UIViewController_OrientationFix ()


@end


@implementation UIViewController (OrientationFix)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}


- (BOOL)shouldAutorotate
{
    return YES;
}


- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


@end
