//
//  Page00.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page00.h"
#import "Globals.h"


@interface Page00 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhoto1;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhoto2;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page00


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Load page assets from Realm
    [self loadItem:_buttonPhoto1];
    [self loadItem:_buttonPhoto2];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


@end
