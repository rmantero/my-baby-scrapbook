//
//  Page10.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page10.h"
#import "Globals.h"


@interface Page10 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page10


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Clip textfield1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(559.66, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 179)];
    [clip1Path addLineToPoint: CGPointMake(781.82, 179)];
    [clip1Path addCurveToPoint: CGPointMake(559.66, 0) controlPoint1: CGPointMake(709.58, 106.96) controlPoint2: CGPointMake(629.17, 45.96)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Exclusion textfield1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(559.66, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(971, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(971, 179)];
    [exclusion1Path addLineToPoint: CGPointMake(781.82, 179)];
    [exclusion1Path addCurveToPoint: CGPointMake(559.66, 0) controlPoint1: CGPointMake(709.58, 106.96) controlPoint2: CGPointMake(629.17, 45.96)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textfield2
    UIBezierPath* clip2Path = UIBezierPath.bezierPath;
    [clip2Path moveToPoint: CGPointMake(829.77, -0)];
    [clip2Path addLineToPoint: CGPointMake(0, -0)];
    [clip2Path addLineToPoint: CGPointMake(0, 69)];
    [clip2Path addLineToPoint: CGPointMake(884.66, 69)];
    [clip2Path addCurveToPoint: CGPointMake(829.77, -0) controlPoint1: CGPointMake(867.71, 45.22) controlPoint2: CGPointMake(849.26, 22.19)];
    [clip2Path closePath];
    clip2Path.miterLimit = 4;
    clip2Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip2Path fill];
    _textview2.visibleArea = clip2Path;

    //Exclusion textfield2
    UIBezierPath* exclusion2Path = UIBezierPath.bezierPath;
    [exclusion2Path moveToPoint: CGPointMake(829.77, -0)];
    [exclusion2Path addLineToPoint: CGPointMake(971, -0)];
    [exclusion2Path addLineToPoint: CGPointMake(971, 69)];
    [exclusion2Path addLineToPoint: CGPointMake(884.66, 69)];
    [exclusion2Path addCurveToPoint: CGPointMake(829.77, -0) controlPoint1: CGPointMake(867.71, 45.22) controlPoint2: CGPointMake(849.26, 22.19)];
    [exclusion2Path closePath];
    exclusion2Path.miterLimit = 4;
    exclusion2Path.usesEvenOddFillRule = YES;
    _textview2.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textfield3
    UIBezierPath* clip3Path = UIBezierPath.bezierPath;
    [clip3Path moveToPoint: CGPointMake(917.88, -0)];
    [clip3Path addLineToPoint: CGPointMake(0, -0)];
    [clip3Path addLineToPoint: CGPointMake(0, 39.22)];
    [clip3Path addLineToPoint: CGPointMake(0.08, 69)];
    [clip3Path addLineToPoint: CGPointMake(952.82, 69)];
    [clip3Path addCurveToPoint: CGPointMake(917.88, -0) controlPoint1: CGPointMake(942.9, 45.49) controlPoint2: CGPointMake(931.14, 22.46)];
    [clip3Path closePath];
    clip3Path.miterLimit = 4;
    clip3Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip3Path fill];
    _textview3.visibleArea = clip3Path;

    //Exclusion textfield3
    UIBezierPath* exclusion3Path = UIBezierPath.bezierPath;
    [exclusion3Path moveToPoint: CGPointMake(917.88, -0)];
    [exclusion3Path addLineToPoint: CGPointMake(971, -0)];
    [exclusion3Path addLineToPoint: CGPointMake(971, 69)];
    [exclusion3Path addLineToPoint: CGPointMake(952.82, 69)];
    [exclusion3Path addCurveToPoint: CGPointMake(917.88, -0) controlPoint1: CGPointMake(942.9, 45.49) controlPoint2: CGPointMake(931.14, 22.46)];
    [exclusion3Path closePath];
    [exclusion3Path moveToPoint: CGPointMake(0.08, 69)];
    [exclusion3Path addLineToPoint: CGPointMake(0, 69)];
    [exclusion3Path addLineToPoint: CGPointMake(0, 39.22)];
    [exclusion3Path addLineToPoint: CGPointMake(0.08, 69)];
    [exclusion3Path closePath];
    exclusion3Path.miterLimit = 4;
    exclusion3Path.usesEvenOddFillRule = YES;
    _textview3.textContainer.exclusionPaths = @[exclusion3Path];

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
