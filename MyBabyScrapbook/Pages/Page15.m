//
//  Page15.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page15.h"
#import "Globals.h"


@interface Page15 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page15


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(150.69, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 545)];
    [exclusion1Path addLineToPoint: CGPointMake(198.02, 545)];
    [exclusion1Path addCurveToPoint: CGPointMake(59.5, 249) controlPoint1: CGPointMake(113.36, 474.29) controlPoint2: CGPointMake(59.5, 367.93)];
    [exclusion1Path addCurveToPoint: CGPointMake(150.69, 0) controlPoint1: CGPointMake(59.5, 154.08) controlPoint2: CGPointMake(93.81, 67.17)];
    [exclusion1Path closePath];
    [exclusion1Path moveToPoint: CGPointMake(739.31, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(890, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(890, 545)];
    [exclusion1Path addLineToPoint: CGPointMake(691.98, 545)];
    [exclusion1Path addCurveToPoint: CGPointMake(830.5, 249) controlPoint1: CGPointMake(776.64, 474.29) controlPoint2: CGPointMake(830.5, 367.93)];
    [exclusion1Path addCurveToPoint: CGPointMake(739.31, 0) controlPoint1: CGPointMake(830.5, 154.08) controlPoint2: CGPointMake(796.19, 67.17)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;

    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(691.98, 544.5)];
    [clip1Path addCurveToPoint: CGPointMake(830.5, 248.5) controlPoint1: CGPointMake(776.64, 473.79) controlPoint2: CGPointMake(830.5, 367.43)];
    [clip1Path addCurveToPoint: CGPointMake(739.31, -0.5) controlPoint1: CGPointMake(830.5, 153.58) controlPoint2: CGPointMake(796.19, 66.67)];
    [clip1Path addLineToPoint: CGPointMake(150.69, -0.5)];
    [clip1Path addCurveToPoint: CGPointMake(59.5, 248.5) controlPoint1: CGPointMake(93.81, 66.67) controlPoint2: CGPointMake(59.5, 153.58)];
    [clip1Path addCurveToPoint: CGPointMake(198.02, 544.5) controlPoint1: CGPointMake(59.5, 367.43) controlPoint2: CGPointMake(113.36, 473.79)];
    [clip1Path addLineToPoint: CGPointMake(691.98, 544.5)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;

    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview1.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];

    //Load page assets from Realm
    [self loadItem:_textview1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
