//
//  Page12.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page12.h"
#import "Globals.h"


@interface Page12 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonTeeth;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page12


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    _textview1.drawWithoutFrame = YES;

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(606.85, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 420)];
    [clip1Path addLineToPoint: CGPointMake(819.5, 420)];
    [clip1Path addLineToPoint: CGPointMake(688, 420)];
    [clip1Path addCurveToPoint: CGPointMake(473, 201.5) controlPoint1: CGPointMake(568.94, 418.1) controlPoint2: CGPointMake(473, 321.01)];
    [clip1Path addCurveToPoint: CGPointMake(606.85, 0) controlPoint1: CGPointMake(473, 110.84) controlPoint2: CGPointMake(528.22, 33.07)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(606.85, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(951, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(951, 420)];
    [exclusion1Path addLineToPoint: CGPointMake(819.5, 420)];
    [exclusion1Path addLineToPoint: CGPointMake(688, 420)];
    [exclusion1Path addCurveToPoint: CGPointMake(473, 201.5) controlPoint1: CGPointMake(568.94, 418.1) controlPoint2: CGPointMake(473, 321.01)];
    [exclusion1Path addCurveToPoint: CGPointMake(606.85, 0) controlPoint1: CGPointMake(473, 110.84) controlPoint2: CGPointMake(528.22, 33.07)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Textfield
    _textfield1.maxLength = 1;
    _textfield1.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_labelTitle];
    [self loadItem:_textfield1];

    //Load first tooth
    for (UIButton *b in _buttonTeeth)
    {
        b.selected = NO;
    }
    int ft = [defaults integerForKey:@"firstTooth"];
    [_buttonTeeth[ft] setSelected:YES];
}


/**
 *  Select first tooth
 *
 */
- (IBAction)buttonTooth:(id)sender
{
    UIButton *button = (UIButton *)sender;

    for (UIButton *b in _buttonTeeth)
    {
        b.selected = NO;
    }

    button.selected = YES;

    //Save selection
    [defaults setInteger:button.tag forKey:@"firstTooth"];
    [defaults synchronize];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
