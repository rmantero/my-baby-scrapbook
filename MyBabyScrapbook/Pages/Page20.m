//
//  Page20.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 13/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page20.h"
#import "Globals.h"


@interface Page20 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page20


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -


    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
