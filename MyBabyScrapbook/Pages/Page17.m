//
//  Page17.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 10/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page17.h"
#import "Globals.h"


@interface Page17 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page17


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_buttonImage1];
    [self loadItem:_textfield1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
