//
//  Page01.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page01.h"
#import "Globals.h"


@interface Page01 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield3;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhoto1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page01


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusionPath = UIBezierPath.bezierPath;
    [exclusionPath moveToPoint: CGPointMake(623.33, 206.24)];
    [exclusionPath addLineToPoint: CGPointMake(632.76, 90.21)];
    [exclusionPath addLineToPoint: CGPointMake(693.74, 94.41)];
    [exclusionPath addLineToPoint: CGPointMake(693.74, 206.24)];
    [exclusionPath addLineToPoint: CGPointMake(623.33, 206.24)];
    [exclusionPath closePath];
    exclusionPath.miterLimit = 4;
    exclusionPath.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusionPath];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(693.74, 206)];
    [clip1Path addLineToPoint: CGPointMake(694, 206)];
    [clip1Path addLineToPoint: CGPointMake(694, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 206)];
    [clip1Path addLineToPoint: CGPointMake(611.35, 206)];
    [clip1Path addLineToPoint: CGPointMake(620.76, 86.21)];
    [clip1Path addLineToPoint: CGPointMake(693.74, 91.41)];
    [clip1Path addLineToPoint: CGPointMake(693.74, 206)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    _textview1.drawWithoutFrame = YES;

    //Configure textfields
    _textfield2.maxLength = 2;
    _textfield2.inputView = [LNNumberpad defaultLNNumberpad];
    _textfield3.maxLength = 2;
    _textfield3.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_buttonPhoto1];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];
    [self loadItem:_textfield3];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
