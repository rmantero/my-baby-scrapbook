//
//  Page07.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page07.h"
#import "Globals.h"


@interface Page07 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page07


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview1.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.2f];
    _textview2.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.2f];
    _textview3.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.2f];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];

    //Labels
    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];
    _label2.text = [baby.babyName uppercaseString];
    _label3.text = [NSString stringWithFormat:  NSLocalizedString(@"page07_label3", nil), baby.babyName];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
