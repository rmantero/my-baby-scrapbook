//
//  Page18.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 11/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page18.h"
#import "Globals.h"


@interface Page18 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page18


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(793.34, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(820, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(820, 29.39)];
    [exclusion1Path addCurveToPoint: CGPointMake(793.34, 0) controlPoint1: CGPointMake(811.51, 19.23) controlPoint2: CGPointMake(802.62, 9.43)];
    [exclusion1Path addLineToPoint: CGPointMake(793.34, 0)];
    [exclusion1Path closePath];
    [exclusion1Path moveToPoint: CGPointMake(107.07, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 165.53)];
    [exclusion1Path addCurveToPoint: CGPointMake(107.07, 0) controlPoint1: CGPointMake(24.07, 103.02) controlPoint2: CGPointMake(60.81, 46.79)];
    [exclusion1Path addLineToPoint: CGPointMake(107.07, 0)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(820, 29.39)];
    [clip1Path addCurveToPoint: CGPointMake(793.34, 0) controlPoint1: CGPointMake(811.51, 19.23) controlPoint2: CGPointMake(802.62, 9.43)];
    [clip1Path addLineToPoint: CGPointMake(107.07, 0)];
    [clip1Path addCurveToPoint: CGPointMake(0, 165.53) controlPoint1: CGPointMake(60.81, 46.79) controlPoint2: CGPointMake(24.07, 103.02)];
    [clip1Path addLineToPoint: CGPointMake(0, 234)];
    [clip1Path addLineToPoint: CGPointMake(820, 234)];
    [clip1Path addLineToPoint: CGPointMake(820, 29.39)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    _textview1.visibleArea = clip1Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_buttonImage1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
