//
//  Page03.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page03.h"
#import "Globals.h"


@interface Page03 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;
@property (weak, nonatomic) IBOutlet CircularButton *buttonPhoto1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page03


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Set exclusion shape of textview1
    UIBezierPath* exclusionPath = UIBezierPath.bezierPath;
    [exclusionPath moveToPoint: CGPointMake(307, 284)];
    [exclusionPath addCurveToPoint: CGPointMake(426, 165) controlPoint1: CGPointMake(372.72, 284) controlPoint2: CGPointMake(426, 230.72)];
    [exclusionPath addCurveToPoint: CGPointMake(307, 46) controlPoint1: CGPointMake(426, 99.28) controlPoint2: CGPointMake(372.72, 46)];
    [exclusionPath addCurveToPoint: CGPointMake(188, 165) controlPoint1: CGPointMake(241.28, 46) controlPoint2: CGPointMake(188, 99.28)];
    [exclusionPath addCurveToPoint: CGPointMake(307, 284) controlPoint1: CGPointMake(188, 230.72) controlPoint2: CGPointMake(241.28, 284)];
    [exclusionPath closePath];
    [exclusionPath moveToPoint: CGPointMake(737.97, 0.14)];
    [exclusionPath addLineToPoint: CGPointMake(678.5, 0.14)];
    [exclusionPath addCurveToPoint: CGPointMake(407.88, 545.66) controlPoint1: CGPointMake(678.5, 0.14) controlPoint2: CGPointMake(868.05, 376.48)];
    [exclusionPath addCurveToPoint: CGPointMake(735.72, 545.66) controlPoint1: CGPointMake(500.31, 545.66) controlPoint2: CGPointMake(735.72, 545.66)];
    [exclusionPath addLineToPoint: CGPointMake(737.97, 0.14)];
    [exclusionPath closePath];
    exclusionPath.miterLimit = 4;
    _textview1.textContainer.exclusionPaths = @[exclusionPath];

    //Set visible area for textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(686.87, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 527)];
    [clip1Path addLineToPoint: CGPointMake(370.47, 527)];
    [clip1Path addCurveToPoint: CGPointMake(721.48, 156.08) controlPoint1: CGPointMake(566.11, 516.38) controlPoint2: CGPointMake(721.48, 354.37)];
    [clip1Path addCurveToPoint: CGPointMake(686.87, 0) controlPoint1: CGPointMake(721.48, 100.05) controlPoint2: CGPointMake(709.08, 47.22)];
    [clip1Path closePath];
    [clip1Path moveToPoint: CGPointMake(308, 271)];
    [clip1Path addCurveToPoint: CGPointMake(427, 152) controlPoint1: CGPointMake(373.72, 271) controlPoint2: CGPointMake(427, 217.72)];
    [clip1Path addCurveToPoint: CGPointMake(308, 33) controlPoint1: CGPointMake(427, 86.28) controlPoint2: CGPointMake(373.72, 33)];
    [clip1Path addCurveToPoint: CGPointMake(189, 152) controlPoint1: CGPointMake(242.28, 33) controlPoint2: CGPointMake(189, 86.28)];
    [clip1Path addCurveToPoint: CGPointMake(308, 271) controlPoint1: CGPointMake(189, 217.72) controlPoint2: CGPointMake(242.28, 271)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    _textview1.drawWithoutFrame = YES;

    //Configure textfields
    _textfield1.maxLength = 2;
    _textfield1.inputView = [LNNumberpad defaultLNNumberpad];
    _textfield2.maxLength = 2;
    _textfield2.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_buttonPhoto1];
    [self loadItem:_textview1];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
