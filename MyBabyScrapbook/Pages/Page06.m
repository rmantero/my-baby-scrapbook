//
//  Page06.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page06.h"
#import "Globals.h"


@interface Page06 ()


@property (weak, nonatomic) IBOutlet UIButton *buttonPhoto1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield3;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *label1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page06


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Title
    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];
    if (baby.babyIsFemale)
    {
        _labelTitle.text = NSLocalizedString(@"page06_title_female", nil);
        _label1.text = NSLocalizedString(@"page06_label1_female", nil);
    }
    else
    {
        _labelTitle.text = NSLocalizedString(@"page06_title_male", nil);
        _label1.text = NSLocalizedString(@"page06_label1_male", nil);
    }

    //Set exclusion shape of textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(332.05, 353.96)];
    [exclusion1Path addLineToPoint: CGPointMake(356.84, 85.99)];
    [exclusion1Path addLineToPoint: CGPointMake(426.18, 91.73)];
    [exclusion1Path addLineToPoint: CGPointMake(426.18, 354.43)];
    [exclusion1Path addLineToPoint: CGPointMake(332.05, 353.96)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Set clip path of textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(425.18, 354)];
    [clip1Path addLineToPoint: CGPointMake(426, 354)];
    [clip1Path addLineToPoint: CGPointMake(426, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, 354)];
    [clip1Path addLineToPoint: CGPointMake(338.78, 354)];
    [clip1Path addLineToPoint: CGPointMake(331.05, 353.96)];
    [clip1Path addLineToPoint: CGPointMake(355.84, 85.99)];
    [clip1Path addLineToPoint: CGPointMake(425.18, 91.73)];
    [clip1Path addLineToPoint: CGPointMake(425.18, 354)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Set exclusion shape of textview2
    UIBezierPath* exclusion2Path = UIBezierPath.bezierPath;
    [exclusion2Path moveToPoint: CGPointMake(-0.09, 132.83)];
    [exclusion2Path addLineToPoint: CGPointMake(111.34, 140.84)];
    [exclusion2Path addLineToPoint: CGPointMake(95.98, 335.4)];
    [exclusion2Path addLineToPoint: CGPointMake(-0.09, 335.17)];
    [exclusion2Path addLineToPoint: CGPointMake(-0.09, 132.83)];
    [exclusion2Path closePath];
    exclusion2Path.miterLimit = 4;
    exclusion2Path.usesEvenOddFillRule = YES;
    _textview2.textContainer.exclusionPaths = @[exclusion2Path];

    //Set clip path of textview2
    UIBezierPath* clip2Path = UIBezierPath.bezierPath;
    [clip2Path moveToPoint: CGPointMake(96.01, 335)];
    [clip2Path addLineToPoint: CGPointMake(449, 335)];
    [clip2Path addLineToPoint: CGPointMake(449, 0)];
    [clip2Path addLineToPoint: CGPointMake(0, 0)];
    [clip2Path addLineToPoint: CGPointMake(0, 132.84)];
    [clip2Path addLineToPoint: CGPointMake(111.34, 140.84)];
    [clip2Path addLineToPoint: CGPointMake(96.01, 335)];
    [clip2Path closePath];
    clip2Path.miterLimit = 4;
    clip2Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip2Path fill];
    _textview2.visibleArea = clip2Path;

    //Configure textfields
    _textfield2.maxLength = 2;
    _textfield2.inputView = [LNNumberpad defaultLNNumberpad];
    _textfield3.placeholder = NSLocalizedString(@"page06_textfield_03_placeholder", nil);

    //Load page assets from Realm
    [self loadItem:_buttonPhoto1];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];
    [self loadItem:_textfield3];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
