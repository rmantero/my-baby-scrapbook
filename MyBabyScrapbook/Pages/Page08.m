//
//  Page08.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page08.h"
#import "Globals.h"


@interface Page08 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage1;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage2;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage3;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage4;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage5;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage6;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage7;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage8;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage9;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage10;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage11;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage12;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage13;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage14;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage15;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page08


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(10, 0.33)];
    [exclusion1Path addCurveToPoint: CGPointMake(150.77, 219.83) controlPoint1: CGPointMake(10, 0.33) controlPoint2: CGPointMake(-14.39, 219.83)];
    [exclusion1Path addCurveToPoint: CGPointMake(519.19, 238.93) controlPoint1: CGPointMake(315.93, 219.83) controlPoint2: CGPointMake(450.92, 183.66)];
    [exclusion1Path addCurveToPoint: CGPointMake(567.52, 351.3) controlPoint1: CGPointMake(551.61, 265.18) controlPoint2: CGPointMake(567.52, 314.9)];
    [exclusion1Path addCurveToPoint: CGPointMake(0.46, 351.3) controlPoint1: CGPointMake(532.67, 351.3) controlPoint2: CGPointMake(0.46, 351.3)];
    [exclusion1Path addLineToPoint: CGPointMake(0.46, 0.33)];
    [exclusion1Path addLineToPoint: CGPointMake(10, 0.33)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(680, 0)];
    [clip1Path addLineToPoint: CGPointMake(680, 352)];
    [clip1Path addLineToPoint: CGPointMake(0, 352)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path closePath];
    [clip1Path moveToPoint: CGPointMake(10, 0.33)];
    [clip1Path addCurveToPoint: CGPointMake(150.77, 219.83) controlPoint1: CGPointMake(10, 0.33) controlPoint2: CGPointMake(-14.39, 219.83)];
    [clip1Path addCurveToPoint: CGPointMake(519.19, 238.93) controlPoint1: CGPointMake(315.93, 219.83) controlPoint2: CGPointMake(450.92, 183.66)];
    [clip1Path addCurveToPoint: CGPointMake(567.52, 351.3) controlPoint1: CGPointMake(551.61, 265.18) controlPoint2: CGPointMake(567.52, 314.9)];
    [clip1Path addCurveToPoint: CGPointMake(0.46, 351.3) controlPoint1: CGPointMake(532.67, 351.3) controlPoint2: CGPointMake(0.46, 351.3)];
    [clip1Path addLineToPoint: CGPointMake(0.46, 0.33)];
    [clip1Path addLineToPoint: CGPointMake(10, 0.33)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Random buttons rotation
//    _buttonImage1.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage2.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage3.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage4.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage5.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage6.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage7.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage8.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage9.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage10.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage11.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage12.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage13.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage14.superview.superview.tag = arc4random_uniform(16) - 8;
//    _buttonImage15.superview.superview.tag = arc4random_uniform(16) - 8;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_labelTitle];
    [self loadItem:_buttonImage1];
    [self loadItem:_buttonImage2];
    [self loadItem:_buttonImage3];
    [self loadItem:_buttonImage4];
    [self loadItem:_buttonImage5];
    [self loadItem:_buttonImage6];
    [self loadItem:_buttonImage7];
    [self loadItem:_buttonImage8];
    [self loadItem:_buttonImage9];
    [self loadItem:_buttonImage10];
    [self loadItem:_buttonImage11];
    [self loadItem:_buttonImage12];
    [self loadItem:_buttonImage13];
    [self loadItem:_buttonImage14];
    [self loadItem:_buttonImage15];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
