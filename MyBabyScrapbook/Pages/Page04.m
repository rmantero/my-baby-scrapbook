//
//  Page04.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page04.h"
#import "Globals.h"


@interface Page04 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet UIImageView *imageviewGirl;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page04


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    _imageviewGirl.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(-20));

    //Set exclusion shape of textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(477.44, 277.22)];
    [exclusion1Path addLineToPoint: CGPointMake(845.94, -0.38)];
    [exclusion1Path addLineToPoint: CGPointMake(951.4, -0.38)];
    [exclusion1Path addLineToPoint: CGPointMake(951.4, 277.22)];
    [exclusion1Path addLineToPoint: CGPointMake(477.44, 277.22)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Set clip path of textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(0.18, 0.54)];
    [clip1Path addLineToPoint: CGPointMake(841.39, 0.54)];
    [clip1Path addLineToPoint: CGPointMake(471.3, 276.12)];
    [clip1Path addLineToPoint: CGPointMake(0.18, 276.12)];
    [clip1Path addLineToPoint: CGPointMake(0.18, 0.54)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Set exclusion shape of textview2
    UIBezierPath* exclusion2Path = UIBezierPath.bezierPath;
    [exclusion2Path moveToPoint: CGPointMake(449.4, 0.62)];
    [exclusion2Path addLineToPoint: CGPointMake(47.94, 299.22)];
    [exclusion2Path addLineToPoint: CGPointMake(1.47, 299.22)];
    [exclusion2Path addLineToPoint: CGPointMake(1.47, 0.62)];
    [exclusion2Path addLineToPoint: CGPointMake(449.4, 0.62)];
    [exclusion2Path closePath];
    exclusion2Path.miterLimit = 4;
    exclusion2Path.usesEvenOddFillRule = YES;
    _textview2.textContainer.exclusionPaths = @[exclusion2Path];

    //Set clip path of textview2
    UIBezierPath* clip2Path = UIBezierPath.bezierPath;
    [clip2Path moveToPoint: CGPointMake(951.39, 301.12)];
    [clip2Path addLineToPoint: CGPointMake(51.18, 301.12)];
    [clip2Path addLineToPoint: CGPointMake(454.28, -0.46)];
    [clip2Path addLineToPoint: CGPointMake(951.39, 0.55)];
    [clip2Path addLineToPoint: CGPointMake(951.39, 301.12)];
    [clip2Path closePath];
    clip2Path.miterLimit = 4;
    clip2Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor]  setFill];
    [clip2Path fill];
    _textview2.visibleArea = clip2Path;

    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
