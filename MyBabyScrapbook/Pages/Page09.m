//
//  Page09.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page09.h"
#import "Globals.h"


@interface Page09 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview5;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page09


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Clip textfield1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(109.09, 0)];
    [clip1Path addLineToPoint: CGPointMake(482, 0)];
    [clip1Path addLineToPoint: CGPointMake(482, 172)];
    [clip1Path addLineToPoint: CGPointMake(0, 172)];
    [clip1Path addLineToPoint: CGPointMake(0, 101.25)];
    [clip1Path addCurveToPoint: CGPointMake(109.09, 0) controlPoint1: CGPointMake(33.2, 66.34) controlPoint2: CGPointMake(69.55, 32.35)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Exclusion textfield1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(109.09, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 0)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 101.25)];
    [exclusion1Path addCurveToPoint: CGPointMake(109.09, 0) controlPoint1: CGPointMake(33.2, 66.34) controlPoint2: CGPointMake(69.55, 32.35)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textfield3
    UIBezierPath* clip3Path = UIBezierPath.bezierPath;
    [clip3Path moveToPoint: CGPointMake(952, 25.05)];
    [clip3Path addLineToPoint: CGPointMake(952, 0)];
    [clip3Path addLineToPoint: CGPointMake(34.11, 0)];
    [clip3Path addCurveToPoint: CGPointMake(0, 50.09) controlPoint1: CGPointMake(22.08, 16.7) controlPoint2: CGPointMake(10.71, 33.42)];
    [clip3Path addLineToPoint: CGPointMake(0, 50.09)];
    [clip3Path addLineToPoint: CGPointMake(0, 69)];
    [clip3Path addLineToPoint: CGPointMake(717.18, 69)];
    [clip3Path addLineToPoint: CGPointMake(714.95, 45.79)];
    [clip3Path addLineToPoint: CGPointMake(952, 25.05)];
    [clip3Path closePath];
    clip3Path.miterLimit = 4;
    clip3Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip3Path fill];
    _textview3.visibleArea = clip3Path;

    //Exclusion textfield3
    UIBezierPath* exclusion3Path = UIBezierPath.bezierPath;
    [exclusion3Path moveToPoint: CGPointMake(952, 27.05)];
    [exclusion3Path addLineToPoint: CGPointMake(717.12, 47.6)];
    [exclusion3Path addLineToPoint: CGPointMake(718.99, 69)];
    [exclusion3Path addLineToPoint: CGPointMake(952, 69)];
    [exclusion3Path addLineToPoint: CGPointMake(952, 27.05)];
    [exclusion3Path closePath];
    [exclusion3Path moveToPoint: CGPointMake(34.11, 0)];
    [exclusion3Path addLineToPoint: CGPointMake(0, 0)];
    [exclusion3Path addLineToPoint: CGPointMake(0, 50.09)];
    [exclusion3Path addCurveToPoint: CGPointMake(34.11, 0) controlPoint1: CGPointMake(10.71, 33.42) controlPoint2: CGPointMake(22.08, 16.7)];
    [exclusion3Path addLineToPoint: CGPointMake(34.11, 0)];
    [exclusion3Path closePath];
    exclusion3Path.miterLimit = 4;
    exclusion3Path.usesEvenOddFillRule = YES;
    _textview3.textContainer.exclusionPaths = @[exclusion3Path];

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;
    _textview5.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_buttonImage1];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textview5];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
