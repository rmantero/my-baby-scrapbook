//
//  Page13.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page13.h"
#import "Globals.h"


@interface Page13 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview5;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview6;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page13


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Random rotation
//    _textview1.superview.tag = arc4random_uniform(10) - 5;
//    _textview2.superview.tag = arc4random_uniform(10) - 5;
//    _textview3.superview.tag = arc4random_uniform(10) - 5;
//    _textview4.superview.tag = arc4random_uniform(10) - 5;
//    _textview5.superview.tag = arc4random_uniform(10) - 5;
//    _textview6.superview.tag = arc4random_uniform(10) - 5;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textview5];
    [self loadItem:_textview6];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
