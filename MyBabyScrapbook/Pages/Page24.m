//
//  Page24.m
//  MyBabyScrapbook
//
//  Created by Richie on 20/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page24.h"
#import "Globals.h"


@interface Page24 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page24


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath *exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint:CGPointMake(0, 0)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 0)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 608)];
    [exclusion1Path addLineToPoint:CGPointMake(0, 608)];
    [exclusion1Path addLineToPoint:CGPointMake(0, 0)];
    [exclusion1Path closePath];
    [exclusion1Path moveToPoint:CGPointMake(877, 412.36)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 185.19)];
    [exclusion1Path addCurveToPoint:CGPointMake(847.71, 0) controlPoint1:CGPointMake(869.11, 109.79) controlPoint2:CGPointMake(858.86, 41.46)];
    [exclusion1Path addLineToPoint:CGPointMake(847.71, 0)];
    [exclusion1Path addLineToPoint:CGPointMake(40.33, 0)];
    [exclusion1Path addCurveToPoint:CGPointMake(0, 256.69) controlPoint1:CGPointMake(24.97, 54.86) controlPoint2:CGPointMake(9.77, 155.02)];
    [exclusion1Path addLineToPoint:CGPointMake(-0, 562.14)];
    [exclusion1Path addCurveToPoint:CGPointMake(8.32, 575.39) controlPoint1:CGPointMake(2.34, 568.52) controlPoint2:CGPointMake(5.11, 573.03)];
    [exclusion1Path addCurveToPoint:CGPointMake(650.79, 599.27) controlPoint1:CGPointMake(41.23, 599.49) controlPoint2:CGPointMake(403.78, 607.45)];
    [exclusion1Path addLineToPoint:CGPointMake(548, 551.33)];
    [exclusion1Path addLineToPoint:CGPointMake(609.45, 199)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 248.13)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 412.36)];
    [exclusion1Path addLineToPoint:CGPointMake(877, 412.36)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;

    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[ exclusion1Path ];

    //Clip textview1
    UIBezierPath *clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint:CGPointMake(877, 418.36)];
    [clip1Path addLineToPoint:CGPointMake(877, 191.19)];
    [clip1Path addCurveToPoint:CGPointMake(847.71, 6) controlPoint1:CGPointMake(869.11, 115.79) controlPoint2:CGPointMake(858.86, 47.46)];
    [clip1Path addLineToPoint:CGPointMake(847.71, 6)];
    [clip1Path addLineToPoint:CGPointMake(40.33, 6)];
    [clip1Path addCurveToPoint:CGPointMake(0, 262.69) controlPoint1:CGPointMake(24.97, 60.86) controlPoint2:CGPointMake(9.77, 161.02)];
    [clip1Path addLineToPoint:CGPointMake(-0, 568.14)];
    [clip1Path addCurveToPoint:CGPointMake(8.32, 581.39) controlPoint1:CGPointMake(2.34, 574.52) controlPoint2:CGPointMake(5.11, 579.03)];
    [clip1Path addCurveToPoint:CGPointMake(650.79, 605.27) controlPoint1:CGPointMake(41.23, 605.49) controlPoint2:CGPointMake(403.78, 613.45)];
    [clip1Path addLineToPoint:CGPointMake(548, 557.33)];
    [clip1Path addLineToPoint:CGPointMake(609.45, 205)];
    [clip1Path addLineToPoint:CGPointMake(877, 254.13)];
    [clip1Path addLineToPoint:CGPointMake(877, 418.36)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_buttonImage1];
    [self loadItem:_labelTitle];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


@end
