//
//  Page02.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page02.h"
#import "Globals.h"


@interface Page02 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhoto1;
@property (weak, nonatomic) IBOutlet UILabel *label1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page02


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion path
    UIBezierPath* exclusionPath = [UIBezierPath bezierPathWithRect: CGRectMake(319, 87, 28, 70)];
    [exclusionPath closePath];
    exclusionPath.miterLimit = 4;
    exclusionPath.usesEvenOddFillRule = YES;
    _textview4.textContainer.exclusionPaths = @[exclusionPath];

    //Load page assets from Realm
    [self loadItem:_buttonPhoto1];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_label1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
