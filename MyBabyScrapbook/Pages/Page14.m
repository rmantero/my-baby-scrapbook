//
//  Page14.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page14.h"
#import "Globals.h"


@interface Page14 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page14


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview4
    UIBezierPath* exclusion4Path = UIBezierPath.bezierPath;
    [exclusion4Path moveToPoint: CGPointMake(0, 0)];
    [exclusion4Path addLineToPoint: CGPointMake(670, 0)];
    [exclusion4Path addLineToPoint: CGPointMake(670, 177)];
    [exclusion4Path addLineToPoint: CGPointMake(0, 177)];
    [exclusion4Path addLineToPoint: CGPointMake(0, 0)];
    [exclusion4Path closePath];
    [exclusion4Path moveToPoint: CGPointMake(225.72, 0)];
    [exclusion4Path addCurveToPoint: CGPointMake(117.64, 75.06) controlPoint1: CGPointMake(180.04, 31.26) controlPoint2: CGPointMake(148.6, 64.98)];
    [exclusion4Path addCurveToPoint: CGPointMake(0, 95.03) controlPoint1: CGPointMake(76.06, 88.6) controlPoint2: CGPointMake(50.54, 96.8)];
    [exclusion4Path addLineToPoint: CGPointMake(0, 177)];
    [exclusion4Path addLineToPoint: CGPointMake(670, 177)];
    [exclusion4Path addLineToPoint: CGPointMake(670, 0)];
    [exclusion4Path addLineToPoint: CGPointMake(225.72, 0)];
    [exclusion4Path closePath];
    exclusion4Path.miterLimit = 4;
    exclusion4Path.usesEvenOddFillRule = YES;
    _textview4.textContainer.exclusionPaths = @[exclusion4Path];

    //Clip textview4
    UIBezierPath* clip4Path = UIBezierPath.bezierPath;
    [clip4Path moveToPoint: CGPointMake(225.72, -0)];
    [clip4Path addCurveToPoint: CGPointMake(117.64, 75.06) controlPoint1: CGPointMake(180.04, 31.26) controlPoint2: CGPointMake(148.6, 64.98)];
    [clip4Path addCurveToPoint: CGPointMake(-0, 95.03) controlPoint1: CGPointMake(76.06, 88.6) controlPoint2: CGPointMake(50.54, 96.8)];
    [clip4Path addLineToPoint: CGPointMake(0, 177)];
    [clip4Path addLineToPoint: CGPointMake(670, 177)];
    [clip4Path addLineToPoint: CGPointMake(670, 0)];
    [clip4Path addLineToPoint: CGPointMake(225.72, -0)];
    [clip4Path closePath];
    clip4Path.miterLimit = 4;
    clip4Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip4Path fill];
    _textview4.visibleArea = clip4Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;

    //Textfield
    _textfield1.maxLength = 2;
    _textfield1.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];
    [self loadItem:_buttonImage1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
