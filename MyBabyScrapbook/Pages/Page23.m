//
//  Page23.m
//  MyBabyScrapbook
//
//  Created by Richie on 18/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page23.h"
#import "Globals.h"


@interface Page23 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage2;
@property (weak, nonatomic) IBOutlet UILabel *label1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page23


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(370, 320.52)];
    [exclusion1Path addLineToPoint: CGPointMake(370, 545.01)];
    [exclusion1Path addLineToPoint: CGPointMake(297.85, 346.78)];
    [exclusion1Path addLineToPoint: CGPointMake(370, 320.52)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(370, 320.52)];
    [clip1Path addLineToPoint: CGPointMake(370, 0)];
    [clip1Path addLineToPoint: CGPointMake(0, -0)];
    [clip1Path addLineToPoint: CGPointMake(-0, 604)];
    [clip1Path addLineToPoint: CGPointMake(370, 604)];
    [clip1Path addLineToPoint: CGPointMake(370, 545.01)];
    [clip1Path addLineToPoint: CGPointMake(297.85, 346.78)];
    [clip1Path addLineToPoint: CGPointMake(370, 320.52)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_buttonImage1];
    [self loadItem:_buttonImage2];
    [self loadItem:_label1];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
