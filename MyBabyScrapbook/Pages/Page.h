//
//  Page.h
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Globals.h"

@class BabyModel;


@interface Page : UIViewController
{
    NSString *originalText;
}


@property (nonatomic, weak) THNotesTextView *textviewEdited;
@property (nonatomic, weak) UIButton *buttonEdited;
@property (nonatomic, weak) Page *pageEdited;


- (void)closeKeyboard;
- (void)clearTextview;
- (void)confirmChangesInTextview;
- (void)cancelTextEditingInTextview;
- (void)getPhotoFromCamera;
- (void)getPhotoFromGallery;
- (void)deletePhoto;
- (void)showPhotoPicker;
- (void)loadItem:(id)item;


@end