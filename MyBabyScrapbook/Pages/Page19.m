//
//  Page19.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 12/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page19.h"
#import "Globals.h"


@interface Page19 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage2;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page19


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(627.64, 117.57)];
    [exclusion1Path addLineToPoint: CGPointMake(510.13, 0.36)];
    [exclusion1Path addLineToPoint: CGPointMake(627.82, 0.13)];
    [exclusion1Path addLineToPoint: CGPointMake(627.64, 117.57)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(0, 0)];
    [clip1Path addLineToPoint: CGPointMake(628, 0)];
    [clip1Path addLineToPoint: CGPointMake(628, 118)];
    [clip1Path addLineToPoint: CGPointMake(0, 118)];
    [clip1Path addLineToPoint: CGPointMake(0, 0)];
    [clip1Path closePath];
    [clip1Path moveToPoint: CGPointMake(627.64, 117.57)];
    [clip1Path addLineToPoint: CGPointMake(510.13, 0.36)];
    [clip1Path addLineToPoint: CGPointMake(627.82, 0.13)];
    [clip1Path addLineToPoint: CGPointMake(627.64, 117.57)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Exclusion textview2
    UIBezierPath* exclusion2Path = UIBezierPath.bezierPath;
    [exclusion2Path moveToPoint: CGPointMake(612, 39.03)];
    [exclusion2Path addLineToPoint: CGPointMake(612, 117.57)];
    [exclusion2Path addLineToPoint: CGPointMake(569.64, 117.57)];
    [exclusion2Path addLineToPoint: CGPointMake(453.13, 0.36)];
    [exclusion2Path addLineToPoint: CGPointMake(611.82, 0.13)];
    [exclusion2Path addLineToPoint: CGPointMake(612, 39.03)];
    [exclusion2Path closePath];
    exclusion2Path.miterLimit = 4;
    exclusion2Path.usesEvenOddFillRule = YES;
    _textview2.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textview2
    UIBezierPath* clip2Path = UIBezierPath.bezierPath;
    [clip2Path moveToPoint: CGPointMake(612, 39.03)];
    [clip2Path addLineToPoint: CGPointMake(612, 0)];
    [clip2Path addLineToPoint: CGPointMake(0, 0)];
    [clip2Path addLineToPoint: CGPointMake(0, 118)];
    [clip2Path addLineToPoint: CGPointMake(612, 118)];
    [clip2Path addLineToPoint: CGPointMake(612, 117.57)];
    [clip2Path addLineToPoint: CGPointMake(569.64, 117.57)];
    [clip2Path addLineToPoint: CGPointMake(453.13, 0.36)];
    [clip2Path addLineToPoint: CGPointMake(611.82, 0.13)];
    [clip2Path addLineToPoint: CGPointMake(612, 39.03)];
    [clip2Path closePath];
    clip2Path.miterLimit = 4;
    clip2Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip2Path fill];
    _textview2.visibleArea = clip2Path;

    //Exclusion textview3
    UIBezierPath* exclusion3Path = UIBezierPath.bezierPath;
    [exclusion3Path moveToPoint: CGPointMake(755.36, 117.57)];
    [exclusion3Path addLineToPoint: CGPointMake(737.64, 117.57)];
    [exclusion3Path addLineToPoint: CGPointMake(621.13, 0.36)];
    [exclusion3Path addLineToPoint: CGPointMake(754.82, 0.13)];
    [exclusion3Path addLineToPoint: CGPointMake(755.36, 117.57)];
    [exclusion3Path closePath];
    exclusion3Path.miterLimit = 4;
    exclusion3Path.usesEvenOddFillRule = YES;
    _textview3.textContainer.exclusionPaths = @[exclusion3Path];

    //Clip textview3
    UIBezierPath* clip3Path = UIBezierPath.bezierPath;
    [clip3Path moveToPoint: CGPointMake(755, 39.03)];
    [clip3Path addLineToPoint: CGPointMake(755, 0)];
    [clip3Path addLineToPoint: CGPointMake(0, 0)];
    [clip3Path addLineToPoint: CGPointMake(0, 117)];
    [clip3Path addLineToPoint: CGPointMake(737.07, 117)];
    [clip3Path addLineToPoint: CGPointMake(621.13, 0.36)];
    [clip3Path addLineToPoint: CGPointMake(754.82, 0.13)];
    [clip3Path addLineToPoint: CGPointMake(755, 39.03)];
    [clip3Path closePath];
    clip3Path.miterLimit = 4;
    clip3Path.usesEvenOddFillRule = YES;
    _textview3.visibleArea = clip3Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_buttonImage1];
    [self loadItem:_buttonImage2];
    [self loadItem:_labelTitle];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
