//
//  Page05.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page05.h"
#import "Globals.h"


@interface Page05 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield3;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield4;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield5;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield6;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page05


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Set exclusion shape of textview1
    UIBezierPath* exclusionPath = UIBezierPath.bezierPath;
    [exclusionPath moveToPoint: CGPointMake(4.84, 0)];
    [exclusionPath addLineToPoint: CGPointMake(0, 0)];
    [exclusionPath addLineToPoint: CGPointMake(0, 14.05)];
    [exclusionPath addCurveToPoint: CGPointMake(4.84, 0) controlPoint1: CGPointMake(1.51, 9.32) controlPoint2: CGPointMake(3.12, 4.63)];
    [exclusionPath closePath];
    [exclusionPath moveToPoint: CGPointMake(544, 321.15)];
    [exclusionPath addLineToPoint: CGPointMake(544, 424)];
    [exclusionPath addLineToPoint: CGPointMake(365.89, 424)];
    [exclusionPath addCurveToPoint: CGPointMake(544, 321.15) controlPoint1: CGPointMake(436.34, 410.15) controlPoint2: CGPointMake(498.47, 373.11)];
    [exclusionPath closePath];
    [exclusionPath moveToPoint: CGPointMake(242.11, 424)];
    [exclusionPath addLineToPoint: CGPointMake(0, 424)];
    [exclusionPath addLineToPoint: CGPointMake(0, 207.95)];
    [exclusionPath addCurveToPoint: CGPointMake(242.11, 424) controlPoint1: CGPointMake(34.93, 317.57) controlPoint2: CGPointMake(127.51, 401.47)];
    [exclusionPath closePath];
    exclusionPath.miterLimit = 4;
    exclusionPath.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusionPath];

    //Set clip path of textview1
    UIBezierPath* clipPath = UIBezierPath.bezierPath;
    [clipPath moveToPoint: CGPointMake(365.89, 424)];
    [clipPath addCurveToPoint: CGPointMake(544, 321.15) controlPoint1: CGPointMake(436.34, 410.15) controlPoint2: CGPointMake(498.47, 373.11)];
    [clipPath addLineToPoint: CGPointMake(544, 0)];
    [clipPath addLineToPoint: CGPointMake(4.84, 0)];
    [clipPath addCurveToPoint: CGPointMake(0, 14.05) controlPoint1: CGPointMake(3.12, 4.63) controlPoint2: CGPointMake(1.51, 9.32)];
    [clipPath addLineToPoint: CGPointMake(0, 207.95)];
    [clipPath addCurveToPoint: CGPointMake(242.11, 424) controlPoint1: CGPointMake(34.93, 317.57) controlPoint2: CGPointMake(127.51, 401.47)];
    [clipPath addLineToPoint: CGPointMake(365.89, 424)];
    [clipPath closePath];
    clipPath.miterLimit = 4;
    clipPath.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clipPath fill];
    _textview1.visibleArea = clipPath;

    _textview1.drawWithoutFrame = YES;

    //Configure textfields
    _textfield3.maxLength = 4;
    _textfield3.inputView = [LNNumberpad defaultLNNumberpad];
    _textfield4.maxLength = 2;
    _textfield4.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];
    [self loadItem:_textfield3];
    [self loadItem:_textfield4];
    [self loadItem:_textfield5];
    [self loadItem:_textfield6];
}


/**
 *  Needed to allow this textfield resize dynamically as typing
 *
 */
- (IBAction) textFieldDidChange: (UITextField*) textField
{
    [textField invalidateIntrinsicContentSize];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
