//
//  Page.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "HFImageEditorViewController.h"
#import "BabySelector.h"
#import "CircularButton.h"


@interface Page () <UITextViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate>
{
    UITapGestureRecognizer *tap;
    NSArray *originalExclusionPath;
    UIBezierPath *originalClipPath;
    UIAlertController *actionSheet;
    UIImagePickerController *imagePicker;
    HFImageEditorViewController *imageEditor;
    ALAssetsLibrary *library;
    int originalZOrder;
}


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page


- (void)viewDidLoad
{
    [super viewDidLoad];

    //Set image editor
    imageEditor = (HFImageEditorViewController *)[storyboard instantiateViewControllerWithIdentifier:@"imageEditor"];
    imageEditor.checkBounds = YES;
    imageEditor.rotateEnabled = YES;
    imageEditor.scaleEnabled = YES;
    imageEditor.tapToResetEnabled = YES;

    //What to do when finished edition?
    __weak typeof(self) weakSelf = self;
    imageEditor.doneCallback = ^(UIImage *editedImage, BOOL canceled) {

      if (!canceled)
      {
          //Save the cropped image to Realm
          PhotoModel *photo = [[PhotoModel alloc] init];
          photo.photoId = [NSString stringWithFormat:@"%@_%d", NSStringFromClass([weakSelf.pageEdited class]), weakSelf.buttonEdited.tag];
          photo.photoData = UIImageJPEGRepresentation(editedImage, 0.8f);
          [realm beginWriteTransaction];
          [PhotoModel createOrUpdateInRealm:realm withValue:photo];
          [realm commitWriteTransaction];

          //Close picker
          [weakSelf closePicker];
      }
      else
      {
          //Go to picker root
          [weakSelf goToPickerRoot];
      }
    };
}


#pragma mark - System
#pragma mark -


/**
 *  Set status bar color
 *
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


/**
 *  Hides status bar in iOS7
 *
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/**
 *  Define allowed orientations
 *
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


/**
 *  Enable/Disable autorotation
 *
 */
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


#pragma mark - Actions
#pragma mark -


- (IBAction)photo:(id)sender
{
    //Get button
    UIButton *button = (UIButton *)sender;

    //Pass button to superclass to identify resource in db and updating UI
    self.buttonEdited = button;

    //Show source popup
    [self showPhotoPicker];
}


#pragma mark - Methods
#pragma mark -


/**
 *  Show a full screen view with the photo
 *
 */
- (void)preview:(UIGestureRecognizer *)gesture
{
    //Get button containing the gesture and check if it has a photo to show
    UIButton *button = (UIButton *)gesture.view;
    if (button.currentBackgroundImage == nil)
    {
        return;
    }

    //Close index if open
    [pageController closeMenu];

    //Avoid horizontal scroll from page manager
    [Helper pagingEnabled:NO];

    //Hide index button
    buttonIndex.hidden = YES;

    if (![self.view.subviews containsObject:blurDarkView])
    {
        blurDarkView.frame = self.view.frame;
        [self.view addSubview:blurDarkView];
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePreview)];
        [blurDarkView addGestureRecognizer:tap];

        UIView *frameView = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width / 2) - (self.view.frame.size.height / 2), 0.0, self.view.frame.size.height, self.view.frame.size.height)];
        frameView.backgroundColor = [UIColor whiteColor];
        [blurDarkView addSubview:frameView];

        UIImageView *preview = [[UIImageView alloc] initWithFrame:CGRectMake(frameView.frame.origin.x + 10, frameView.frame.origin.y + 10, frameView.frame.size.width - 20, frameView.frame.size.height - 20)];
        preview.contentMode = UIViewContentModeScaleToFill;
        preview.image = button.currentBackgroundImage;
        [blurDarkView addSubview:preview];
    }
}


/**
 *  Close full screen photo preview
 */
- (void)closePreview
{
    if ([self.view.subviews containsObject:blurDarkView])
    {
        if ([blurDarkView.gestureRecognizers containsObject:tap])
        {
            [blurDarkView removeGestureRecognizer:tap];
        }

        [blurDarkView removeFromSuperview];
    }

    //Enable horizontal scroll from page manager
    [Helper pagingEnabled:YES];

    //Show index button
    buttonIndex.hidden = NO;
}


/**
 *  Load and configure page assets on loading
 *
 *  @param item Must be UIButton, LPlaceholderTextView or SDCTextField
 */
- (void)loadItem:(id)item
{
    if ([item isMemberOfClass:[CircularButton class]])
    {
        CircularButton *button = (CircularButton *)item;
        UIImage *image = nil;

        RLMResults *results = [PhotoModel objectsInRealm:realm where:@"photoId == %@", [NSString stringWithFormat:@"%@_%d", NSStringFromClass([_pageEdited class]), button.tag]];
        if (results)
        {
            PhotoModel *photo = (PhotoModel *)results.firstObject;

            if (photo && photo.photoData != nil)
            {
                image = [UIImage imageWithData:photo.photoData];
            }
        }

        [button setBackgroundImage:image forState:UIControlStateNormal];

        //add border and round it
        button.superview.layer.borderWidth = 1.0f;
        button.superview.layer.borderColor = kColor_DarkOverlay.CGColor;
        button.superview.layer.cornerRadius = button.frame.size.height / 2;
        button.superview.layer.masksToBounds = YES;

        //Photo view rotation
        if (button.superview.superview.tag != 0)
        {
            button.superview.superview.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(button.superview.superview.tag));
        }

        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(preview:)];
        longTap.minimumPressDuration = 1.0f;
        [button addGestureRecognizer:longTap];
    }
    else if ([item isMemberOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)item;
        UIImage *image = nil;

        RLMResults *results = [PhotoModel objectsInRealm:realm where:@"photoId == %@", [NSString stringWithFormat:@"%@_%d", NSStringFromClass([_pageEdited class]), button.tag]];
        if (results)
        {
            PhotoModel *photo = (PhotoModel *)results.firstObject;

            if (photo && photo.photoData != nil)
            {
                image = [UIImage imageWithData:photo.photoData];
            }
        }

        [button setBackgroundImage:image forState:UIControlStateNormal];

        //add shadow
        button.superview.layer.shadowColor = [UIColor blackColor].CGColor;
        button.superview.layer.shadowOpacity = 0.3f;
        button.superview.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
        button.superview.layer.shadowRadius = 2.0f;
        button.superview.layer.masksToBounds = NO;

        //Photo view rotation
        if (button.superview.superview.tag != 0)
        {
            button.superview.superview.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(button.superview.superview.tag));
        }

        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(preview:)];
        longTap.minimumPressDuration = 1.0f;
        [button addGestureRecognizer:longTap];
    }
    else if ([item isMemberOfClass:[THNotesTextView class]])
    {
        THNotesTextView *textV = (THNotesTextView *)item;
        NSString *txt = nil;

        if (textV.drawWithoutFrame == NO)
        {
            // Round textview corners
            [Helper roundCornersToTextView:textV.superview];
        }

        //Set line break mode
        textV.textContainer.lineBreakMode = NSLineBreakByWordWrapping;

        //limit lines
        textV.textContainer.maximumNumberOfLines = textV.lastVisibleLine;

        RLMResults *results = [TextViewModel objectsInRealm:realm where:@"textViewId == %@", [NSString stringWithFormat:@"%@_%d", NSStringFromClass([self.pageEdited class]), textV.tag]];

        if (results)
        {
            TextViewModel *tv = (TextViewModel *)results.firstObject;

            if (tv && tv.textViewText != nil)
            {
                txt = tv.textViewText;
            }
        }
        [textV setText:txt];

        //View rotation
        if (textV.superview.tag != 0)
        {
            textV.superview.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(textV.superview.tag));
        }
    }
    else if ([item isMemberOfClass:[UITextView class]])
    {
        UITextView *textV = (UITextView *)item;
        NSString *txt = nil;

        //Set line break mode
        textV.textContainer.lineBreakMode = NSLineBreakByWordWrapping;

        RLMResults *results = [TextViewModel objectsInRealm:realm where:@"textViewId == %@", [NSString stringWithFormat:@"%@_%d", NSStringFromClass([self.pageEdited class]), textV.tag]];

        if (results)
        {
            TextViewModel *tv = (TextViewModel *)results.firstObject;

            if (tv && tv.textViewText != nil)
            {
                txt = tv.textViewText;
            }
        }
        [textV setText:txt];
    }
    else if ([item isMemberOfClass:[SDCTextField class]])
    {
        SDCTextField *textF = (SDCTextField *)item;
        NSString *txt = nil;

        RLMResults *results = [TextFieldModel objectsInRealm:realm where:@"textFieldId == %@", [NSString stringWithFormat:@"%@_%d", NSStringFromClass([self.pageEdited class]), textF.tag]];
        if (results)
        {
            TextFieldModel *tf = (TextFieldModel *)results.firstObject;

            if (tf && tf.textFieldText != nil)
            {
                txt = tf.textFieldText;
            }
        }
        [textF setText:txt];
    }
    else if ([item isMemberOfClass:[UILabel class]])
    {
        UILabel *label = (UILabel *)item;
        //Label rotation
        if (label.tag != 0)
        {
            label.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(label.tag));
        }
    }
}


/**
 *  Close keyboard. Show warning if changes has been made in textview
 */
- (void)closeKeyboard
{
    if (![originalText isEqualToString:_textviewEdited.text])
    {
        [self showAlert];
    }
    else
    {
        [self.view endEditing:YES];
    }
}


/**
 *  Close image picker
 */
- (void)closePicker
{
    [imagePicker dismissViewControllerAnimated:YES completion:nil];
}


/**
 *  Go to initial imagePicker page
 */
- (void)goToPickerRoot
{
    [imagePicker popToRootViewControllerAnimated:YES];
    [imagePicker setNavigationBarHidden:NO animated:YES];
}


/**
 *  Clear text in textview
 */
- (void)clearTextview
{
    _textviewEdited.text = @"";
}


/**
 *  Update database with changes
 */
- (void)confirmChangesInTextview
{
    //Save the textview text to Realm
    TextViewModel *tv = [[TextViewModel alloc] init];
    tv.textViewId = [NSString stringWithFormat:@"%@_%d", NSStringFromClass([self.pageEdited class]), self.textviewEdited.tag];
    tv.textViewText = _textviewEdited.text;

    [realm beginWriteTransaction];
    [TextViewModel createOrUpdateInRealm:realm withValue:tv];
    [realm commitWriteTransaction];

    [self.view endEditing:YES];
}


/**
 *  Discard changes. Show warning.
 */
- (void)cancelTextEditingInTextview
{
    if (![originalText isEqualToString:_textviewEdited.text])
    {
        [self showAlert];
    }
    else
    {
        [self.view endEditing:YES];
    }
}


/**
 *  Alert user. Changes will be lost
 */
- (void)showAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"page_modification_alert_title", nil)
                                                                   message:NSLocalizedString(@"page_modification_alert_message", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"page_modification_alert_accept", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                       //Restore original text
                                                       _textviewEdited.text = originalText;
                                                       [self.view endEditing:YES];
                                                     }];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"page_modification_alert_cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                           return;
                                                         }];

    [alert addAction:actionOK];
    [alert addAction:actionCancel];

    [self presentViewController:alert animated:YES completion:nil];
}


- (void)showPhotoPicker
{
    actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_title", nil)
                                                      message:nil
                                               preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *actionCamera = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_camera", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction *action) {

                                                           [self getPhotoFromCamera];
                                                         }];

    UIAlertAction *actionGallery = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_gallery", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction *action) {

                                                            [self getPhotoFromGallery];
                                                          }];

    UIAlertAction *actionDeletePhoto = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action) {
                                                                [self deletePhoto];
                                                              }];

    [actionSheet addAction:actionCamera];
    [actionSheet addAction:actionGallery];
    if (_buttonEdited.currentBackgroundImage != nil)
    {
        [actionSheet addAction:actionDeletePhoto];
    }
    [actionSheet setModalPresentationStyle:UIModalPresentationPopover];

    UIPopoverPresentationController *popPresenter = [actionSheet popoverPresentationController];
    popPresenter.sourceView = _buttonEdited;
    popPresenter.sourceRect = _buttonEdited.bounds;
    popPresenter.permittedArrowDirections = UIPopoverArrowDirectionAny;

    [self presentViewController:actionSheet animated:YES completion:nil];

    //Save current page to come back to it after dismiss photo selector
    [defaultRealm beginWriteTransaction];

    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];
    baby.babyCurrentPage = pageController.currentIndex;
    [defaultRealm commitWriteTransaction];
}


- (void)getPhotoFromCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = (id)self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.showsCameraControls = YES;
        imagePicker.modalPresentationStyle = UIModalPresentationCurrentContext;

        [self presentViewController:imagePicker animated:YES completion:NULL];

        //Close index if open
        [pageController closeMenu];
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_error", nil)
                                                                            message:NSLocalizedString(@"photopicker_selector_error_no_camera", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_error_ok", nil)
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                               [self dismissViewControllerAnimated:YES
                                                                                        completion:^{
                                                                                        }];
                                                             }];

        [errorAlert addAction:actionCancel];

        [self presentViewController:errorAlert animated:YES completion:nil];
    }
}


- (void)getPhotoFromGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        //Set photo picker
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = (id)self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

        //Present photo picker
        [self presentViewController:imagePicker animated:YES completion:nil];

        //Close index if open
        [pageController closeMenu];
    }
    else
    {
        UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_error", nil)
                                                                            message:NSLocalizedString(@"photopicker_selector_error_no_photo_gallery", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_error_ok", nil)
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {
                                                               [self dismissViewControllerAnimated:YES
                                                                                        completion:^{
                                                                                        }];
                                                             }];

        [errorAlert addAction:actionCancel];

        [self presentViewController:errorAlert animated:YES completion:nil];
    }
}


- (void)deletePhoto
{
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"photopicker_selector_delete", nil)
                                                                        message:NSLocalizedString(@"photopicker_selector_delete_message", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];

    __weak typeof(self) weakSelf = self;
    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete_accept", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {

                                                       //Delete image from Realm
                                                       PhotoModel *photo = [PhotoModel objectInRealm:realm forPrimaryKey:[NSString stringWithFormat:@"%@_%d", NSStringFromClass([weakSelf.pageEdited class]), weakSelf.buttonEdited.tag]];
                                                       if (photo)
                                                       {
                                                           [realm beginWriteTransaction];
                                                           [realm deleteObject:photo];
                                                           [realm commitWriteTransaction];
                                                       }

                                                       //Remove image from button
                                                       [_buttonEdited setBackgroundImage:nil forState:UIControlStateNormal];

                                                       [self dismissViewControllerAnimated:YES
                                                                                completion:^{
                                                                                }];
                                                     }];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"photopicker_selector_delete_cancel", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                           [self dismissViewControllerAnimated:YES
                                                                                    completion:^{
                                                                                    }];
                                                         }];

    [errorAlert addAction:actionOK];
    [errorAlert addAction:actionCancel];

    [self presentViewController:errorAlert animated:YES completion:nil];
}


#pragma mark - UIImagePickerControllerDelegate
#pragma mark -


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];

    library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:assetURL
        resultBlock:^(ALAsset *asset) {
          UIImage *preview = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];

          imageEditor.sourceImage = image;
          imageEditor.previewImage = preview;
          [imageEditor reset:NO];

          [picker pushViewController:imageEditor animated:YES];
          [picker setNavigationBarHidden:YES animated:NO];

        }
        failureBlock:^(NSError *error) {
          NSLog(@"Failed to get asset from library");
        }];

    //Save photo to camera roll
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImage *takenImage = info[UIImagePickerControllerOriginalImage];

        // Request to save the image to camera roll
        [library writeImageToSavedPhotosAlbum:[takenImage CGImage]
                                  orientation:(ALAssetOrientation)[takenImage imageOrientation]
                              completionBlock:^(NSURL *assetURL, NSError *error) {
                                if (error)
                                {
                                    Log(@"ERROR: Photo not saved to library: %@", error.localizedDescription);
                                }
                              }];
    }
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITextViewDelegate
#pragma mark -


/**
 *  Move textview to a predefined frame to edit. Add background with
 *  tap gesture recognizer to end editing and buttons
 */
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    THNotesTextView *tv = (THNotesTextView *)textView;

    //Pass editing textview original values
    self.textviewEdited = tv;
    originalText = tv.text;
    originalExclusionPath = tv.textContainer.exclusionPaths;
    originalClipPath = tv.visibleArea;
    originalZOrder = [self.view.subviews indexOfObject:tv.superview];

    //Enable scroll
    tv.scrollEnabled = YES;

    //Avoid vertical scroll from keyboard manager
    [IQKeyboardManager sharedManager].enable = NO;

    //Avoid horizontal scroll from page manager
    [Helper pagingEnabled:NO];

    //Hide index button
    buttonIndex.hidden = YES;

    //Close index if open
    [pageController closeMenu];

    //Set blur background
    if (![self.view.subviews containsObject:blurView])
    {
        blurView.frame = self.view.frame;
        [self.view addSubview:blurView];
    }

    //Add tap to blur view
    if (![blurView.gestureRecognizers containsObject:tap])
    {
        tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
        [blurView addGestureRecognizer:tap];
    }

    //Save original position to restore after editing
    originalFrame = tv.superview.frame;
    originalRotation = tv.superview.tag;

    //Bring text view to front
    [self.view bringSubviewToFront:tv.superview];

    //Disable autolayout constraints
    tv.superview.translatesAutoresizingMaskIntoConstraints = YES;

    //Remove textcontainter path
    tv.textContainer.exclusionPaths = nil;

    //Remove clip path
    tv.visibleArea = nil;

    //Animate to editing size
    [UIView animateWithDuration:0.2f
        delay:0.0f
        options:UIViewAnimationOptionCurveEaseInOut
        animations:
            ^{
              tv.superview.layer.affineTransform = CGAffineTransformIdentity;
              tv.superview.frame = kTextviewEditViewFrame;
              [tv.superview layoutIfNeeded];

            }
        completion:
            ^(BOOL finished) {
              //Add buttons
              UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectZero];
              [clearButton setTitle:NSLocalizedString(@"textview_clear_text", nil) forState:UIControlStateNormal];
              [clearButton addTarget:self action:@selector(clearTextview) forControlEvents:UIControlEventTouchUpInside];
              clearButton.titleLabel.font = kButtonFont;
              [clearButton setTitleColor:kColor_Blue forState:UIControlStateNormal];
              clearButton.translatesAutoresizingMaskIntoConstraints = NO;
              [blurView addSubview:clearButton];

              NSLayoutConstraint *clearLeading = [NSLayoutConstraint constraintWithItem:clearButton
                                                                              attribute:NSLayoutAttributeLeading
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:textView
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:0];
              [self.view addConstraint:clearLeading];

              NSLayoutConstraint *clearTop = [NSLayoutConstraint constraintWithItem:clearButton
                                                                          attribute:NSLayoutAttributeTop
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:textView
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:35];
              [self.view addConstraint:clearTop];

              UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectZero];
              [cancelButton setTitle:NSLocalizedString(@"textfview_cancel_changes", nil) forState:UIControlStateNormal];
              [cancelButton addTarget:self action:@selector(cancelTextEditingInTextview) forControlEvents:UIControlEventTouchUpInside];
              cancelButton.titleLabel.font = kButtonFont;
              [cancelButton setTitleColor:kColor_Red forState:UIControlStateNormal];
              cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
              [blurView addSubview:cancelButton];

              NSLayoutConstraint *cancelLeading = [NSLayoutConstraint constraintWithItem:cancelButton
                                                                               attribute:NSLayoutAttributeLeft
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:clearButton
                                                                               attribute:NSLayoutAttributeRight
                                                                              multiplier:1.0
                                                                                constant:30];
              [self.view addConstraint:cancelLeading];

              NSLayoutConstraint *cancelY = [NSLayoutConstraint constraintWithItem:cancelButton
                                                                         attribute:NSLayoutAttributeBaseline
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:clearButton
                                                                         attribute:NSLayoutAttributeBaseline
                                                                        multiplier:1.0
                                                                          constant:0];
              [self.view addConstraint:cancelY];

              UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectZero];
              [confirmButton setTitle:NSLocalizedString(@"textview_add_text", nil) forState:UIControlStateNormal];
              [confirmButton addTarget:self action:@selector(confirmChangesInTextview) forControlEvents:UIControlEventTouchUpInside];
              confirmButton.titleLabel.font = kButtonFont;
              [confirmButton setTitleColor:kColor_Green forState:UIControlStateNormal];
              confirmButton.translatesAutoresizingMaskIntoConstraints = NO;
              [blurView addSubview:confirmButton];

              NSLayoutConstraint *confirmTrailing = [NSLayoutConstraint constraintWithItem:confirmButton
                                                                                 attribute:NSLayoutAttributeRight
                                                                                 relatedBy:NSLayoutRelationEqual
                                                                                    toItem:textView
                                                                                 attribute:NSLayoutAttributeRight
                                                                                multiplier:1.0
                                                                                  constant:0];
              [self.view addConstraint:confirmTrailing];

              NSLayoutConstraint *confirmY = [NSLayoutConstraint constraintWithItem:confirmButton
                                                                          attribute:NSLayoutAttributeBaseline
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:cancelButton
                                                                          attribute:NSLayoutAttributeBaseline
                                                                         multiplier:1.0
                                                                           constant:0];
              [self.view addConstraint:confirmY];

              //Set cursor at text end
              dispatch_async(dispatch_get_main_queue(), ^{
                tv.selectedRange = NSMakeRange(tv.text.length, 0);
              });
            }];
}


/**
 *  Restore textview to its original position. Remove background,
 *  tap gesture recognizer and buttons
 */
- (void)textViewDidEndEditing:(UITextView *)textView
{
    THNotesTextView *tv = (THNotesTextView *)textView;

    //Remove gesture recognizer from blur view
    if ([blurView.gestureRecognizers containsObject:tap])
    {
        [blurView removeGestureRecognizer:tap];
    }

    //Remove blur background
    if ([self.view.subviews containsObject:blurView])
    {
        [blurView removeFromSuperview];
    }

    //Restore exclusionPaths
    tv.textContainer.exclusionPaths = originalExclusionPath;

    //Restore clip path
    tv.visibleArea = originalClipPath;

    //Animate to normal size
    [UIView animateWithDuration:0.2f
        delay:0.0f
        options:UIViewAnimationOptionCurveEaseInOut
        animations:
            ^{
              tv.superview.frame = originalFrame;
              tv.superview.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(originalRotation));

              //Restore z position
              [self.view insertSubview:tv.superview atIndex:originalZOrder];
            }

        completion:
            ^(BOOL finished) {
              //Restore keyboard manager
              [IQKeyboardManager sharedManager].enable = YES;

              //Restore page manager
              [Helper pagingEnabled:YES];

              //Hide index button
              buttonIndex.hidden = NO;

              //Disable scroll
              tv.scrollEnabled = NO;
              tv.scrollsToTop = YES;

              //Re-enable autolayout constraints
              tv.superview.translatesAutoresizingMaskIntoConstraints = NO;
            }];
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}


/**
 *  No new lines allowed
 *
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound)
    {
        return YES;
    }

    return NO;
}


#pragma mark - UITextFieldDelegate
#pragma mark -


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //Avoid horizontal scroll from page manager
    [Helper pagingEnabled:NO];

    //Close index if open
    [pageController closeMenu];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //Save the textview text to Realm
    TextFieldModel *tf = [[TextFieldModel alloc] init];
    tf.textFieldId = [NSString stringWithFormat:@"%@_%d", NSStringFromClass([self.pageEdited class]), textField.tag];
    tf.textFieldText = textField.text;

    [realm beginWriteTransaction];
    [TextFieldModel createOrUpdateInRealm:realm withValue:tf];
    [realm commitWriteTransaction];

    //Re-enable horizontal scroll from page manager
    [Helper pagingEnabled:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}


@end
