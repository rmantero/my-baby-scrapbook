//
//  Page16.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page16.h"
#import "Globals.h"


@interface Page16 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview5;
@property (weak, nonatomic) IBOutlet CircularButton *buttonImage1;
@property (weak, nonatomic) IBOutlet UIButton *buttonDummy;
@property (weak, nonatomic) IBOutlet UIButton *buttonThumb;




@end


#pragma mark - Initialization
#pragma mark -


@implementation Page16


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;
    _textview5.drawWithoutFrame = YES;
    _textview1.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    _textview2.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    _textview3.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    _textview4.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];
    _textview5.horizontalLineColor = [UIColor colorWithWhite:1.0f alpha:0.1f];

    //Buttons
    _buttonThumb.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(-4));
    _buttonDummy.layer.affineTransform = CGAffineTransformMakeRotation(DegreesToRadians(4));

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textview5];
    [self loadItem:_buttonImage1];

    _buttonDummy.selected= [defaults integerForKey:@"dummy"];
    _buttonThumb.selected= ![defaults integerForKey:@"dummy"];
}



- (IBAction)dummy:(id)sender
{
    UIButton *button = (UIButton *)sender;

    //Select button
    _buttonThumb.selected = (button.tag == 0);
    _buttonDummy.selected = (button.tag == 1);

    //Save selection
    [defaults setInteger:button.tag forKey:@"dummy"];
    [defaults synchronize];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
