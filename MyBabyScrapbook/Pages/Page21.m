//
//  Page21.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 15/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page21.h"
#import "Globals.h"


@interface Page21 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview5;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page21


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    UIBezierPath* exclusion1Path = UIBezierPath.bezierPath;
    [exclusion1Path moveToPoint: CGPointMake(227, 110.41)];
    [exclusion1Path addLineToPoint: CGPointMake(227, 169)];
    [exclusion1Path addLineToPoint: CGPointMake(166.22, 169)];
    [exclusion1Path addCurveToPoint: CGPointMake(227, 110.41) controlPoint1: CGPointMake(192.57, 157.01) controlPoint2: CGPointMake(214.08, 136.23)];
    [exclusion1Path closePath];
    [exclusion1Path moveToPoint: CGPointMake(61.78, 169)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 169)];
    [exclusion1Path addLineToPoint: CGPointMake(0, 108.36)];
    [exclusion1Path addCurveToPoint: CGPointMake(61.78, 169) controlPoint1: CGPointMake(12.79, 135.14) controlPoint2: CGPointMake(34.74, 156.7)];
    [exclusion1Path closePath];
    exclusion1Path.miterLimit = 4;
    exclusion1Path.usesEvenOddFillRule = YES;
    _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    UIBezierPath* clip1Path = UIBezierPath.bezierPath;
    [clip1Path moveToPoint: CGPointMake(165.56, 169)];
    [clip1Path addCurveToPoint: CGPointMake(227, 109.8) controlPoint1: CGPointMake(192.28, 157) controlPoint2: CGPointMake(214.05, 135.98)];
    [clip1Path addLineToPoint: CGPointMake(227, 0)];
    [clip1Path addLineToPoint: CGPointMake(0.13, 0)];
    [clip1Path addCurveToPoint: CGPointMake(-0, 0.26) controlPoint1: CGPointMake(0.08, 0.09) controlPoint2: CGPointMake(0.04, 0.18)];
    [clip1Path addLineToPoint: CGPointMake(0, 107.74)];
    [clip1Path addCurveToPoint: CGPointMake(62.44, 169) controlPoint1: CGPointMake(12.81, 134.87) controlPoint2: CGPointMake(35.02, 156.69)];
    [clip1Path addLineToPoint: CGPointMake(165.56, 169)];
    [clip1Path closePath];
    clip1Path.miterLimit = 4;
    clip1Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip1Path fill];
    _textview1.visibleArea = clip1Path;

    //Exclusion textview2
    UIBezierPath* exclusion2Path = UIBezierPath.bezierPath;
    [exclusion2Path moveToPoint: CGPointMake(-0, 0.26)];
    [exclusion2Path addLineToPoint: CGPointMake(-0, 29.12)];
    [exclusion2Path addLineToPoint: CGPointMake(0, 29.12)];
    [exclusion2Path addLineToPoint: CGPointMake(0, 169)];
    [exclusion2Path addLineToPoint: CGPointMake(227, 169)];
    [exclusion2Path addLineToPoint: CGPointMake(227, 54.9)];
    [exclusion2Path addLineToPoint: CGPointMake(227, 109.8)];
    [exclusion2Path addCurveToPoint: CGPointMake(165.56, 169) controlPoint1: CGPointMake(214.05, 135.98) controlPoint2: CGPointMake(192.28, 157)];
    [exclusion2Path addLineToPoint: CGPointMake(165.56, 169)];
    [exclusion2Path addLineToPoint: CGPointMake(69.63, 169)];
    [exclusion2Path addCurveToPoint: CGPointMake(71, 149.5) controlPoint1: CGPointMake(70.53, 162.63) controlPoint2: CGPointMake(71, 156.12)];
    [exclusion2Path addCurveToPoint: CGPointMake(0, 29.12) controlPoint1: CGPointMake(71, 97.68) controlPoint2: CGPointMake(42.34, 52.56)];
    [exclusion2Path addLineToPoint: CGPointMake(0, 29.12)];
    [exclusion2Path addLineToPoint: CGPointMake(-0, 0.26)];
    [exclusion2Path closePath];
    exclusion2Path.miterLimit = 4;

    exclusion2Path.usesEvenOddFillRule = YES;
    _textview2.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textview2
    UIBezierPath* clip2Path = UIBezierPath.bezierPath;
    [clip2Path moveToPoint: CGPointMake(-0, 29.12)];
    [clip2Path addLineToPoint: CGPointMake(-0, 0.26)];
    [clip2Path addCurveToPoint: CGPointMake(0.13, 0) controlPoint1: CGPointMake(0.04, 0.18) controlPoint2: CGPointMake(0.08, 0.09)];
    [clip2Path addLineToPoint: CGPointMake(227, 0)];
    [clip2Path addLineToPoint: CGPointMake(227, 109.8)];
    [clip2Path addCurveToPoint: CGPointMake(165.56, 169) controlPoint1: CGPointMake(214.05, 135.98) controlPoint2: CGPointMake(192.28, 157)];
    [clip2Path addLineToPoint: CGPointMake(165.56, 169)];
    [clip2Path addLineToPoint: CGPointMake(69.63, 169)];
    [clip2Path addCurveToPoint: CGPointMake(71, 149.5) controlPoint1: CGPointMake(70.53, 162.63) controlPoint2: CGPointMake(71, 156.12)];
    [clip2Path addCurveToPoint: CGPointMake(-0, 29.12) controlPoint1: CGPointMake(71, 97.68) controlPoint2: CGPointMake(42.34, 52.56)];
    [clip2Path closePath];
    clip2Path.miterLimit = 4;

    clip2Path.usesEvenOddFillRule = YES;
    [[UIColor clearColor] setFill];
    [clip2Path fill];
    _textview2.visibleArea = clip2Path;

    //Exclusion textview3
    _textview3.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textview3
    _textview3.visibleArea = clip2Path;

    //Exclusion textview4
    _textview4.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textview4
    _textview4.visibleArea = clip2Path;

    //Exclusion textview5
    _textview5.textContainer.exclusionPaths = @[exclusion2Path];

    //Clip textview5
    _textview5.visibleArea = clip2Path;

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;
    _textview5.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textview5];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
