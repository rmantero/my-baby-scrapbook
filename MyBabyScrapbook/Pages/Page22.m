//
//  Page22.m
//  MyBabyScrapbook
//
//  Created by Richie on 16/5/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page22.h"
#import "Globals.h"


@interface Page22 ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview4;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview5;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview6;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield2;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow0;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow1;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow2;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow3;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow4;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsRow5;



@end


#pragma mark - Initialization
#pragma mark -


@implementation Page22


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Textviews
    _textview1.drawWithoutFrame = YES;
    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;
    _textview4.drawWithoutFrame = YES;
    _textview5.drawWithoutFrame = YES;
    _textview6.drawWithoutFrame = YES;

    //Textfield
    _textfield1.maxLength = 2;
    _textfield1.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
    [self loadItem:_textview4];
    [self loadItem:_textview5];
    [self loadItem:_textview6];
    [self loadItem:_textfield1];
    [self loadItem:_textfield2];

    //Load stored buttons state
    NSArray *foods = [defaults objectForKey:@"foods"];
    for (int xx = 0; xx < foods.count; xx++)
    {
        //Get row
        NSString *row = [NSString stringWithFormat: @"buttonsRow%d",xx];

        //Deselect and select
        for (int ii=0; ii<3; ii++)
        {
            UIButton *buttonReset = (UIButton *)[self valueForKey:row][ii];
            buttonReset.selected = (ii == [foods[xx] intValue]);
        }
    }
}


- (IBAction)buttonPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int r = button.tag/10;
    int c = button.tag%10;

    NSString *row = [NSString stringWithFormat: @"buttonsRow%d",r];

    //Deselect and select
    for (int ii=0; ii<3; ii++)
    {
        UIButton *buttonReset = (UIButton *)[self valueForKey:row][ii];
        buttonReset.selected = (button.tag == buttonReset.tag);
    }

    //Store current value
    NSMutableArray *foods = [[defaults objectForKey:@"foods"] mutableCopy];
    foods[r] = @(c);
    [defaults setObject:foods forKey:@"foods"];
    [defaults synchronize];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
