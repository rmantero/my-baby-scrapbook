//
//  Page11.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "Page11.h"
#import "Globals.h"


@interface Page11 ()


@property (weak, nonatomic) IBOutlet UIButton *buttonImage;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview2;
@property (weak, nonatomic) IBOutlet THNotesTextView *textview3;


@end


#pragma mark - Initialization
#pragma mark -


@implementation Page11


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    _textview2.drawWithoutFrame = YES;
    _textview3.drawWithoutFrame = YES;

    //Load page assets from Realm
    [self loadItem:_buttonImage];
    [self loadItem:_textview1];
    [self loadItem:_textview2];
    [self loadItem:_textview3];
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    

    [super viewWillDisappear:animated];
}


@end
