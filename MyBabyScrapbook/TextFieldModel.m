//
//  TextFieldModel.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "TextFieldModel.h"


@implementation TextFieldModel


// Specify default values for properties
+ (NSDictionary *)defaultPropertyValues
{
    return
        @{
            @"textFieldId" : @"",
            @"textFieldText" : @""
        };
}


// Specify primary key
+ (NSString *)primaryKey
{
    return @"textFieldId";
}


// Specify properties to ignore (Realm won't persist these)

+ (NSArray *)ignoredProperties
{
    return @[];
}


@end
