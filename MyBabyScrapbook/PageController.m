//
//  PageController.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 8/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "PageController.h"
#import "BabySelector.h"
#import "AppDelegate.h"
#import "Globals.h"


@interface PageController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
{
    UIGestureRecognizer *pan;
    BOOL isForward;
    BOOL transitioning;
}


@end


#pragma mark - Initialization
#pragma mark -


@implementation PageController


- (void)viewDidLoad
{
    [super viewDidLoad];

    pageController = self;

    [self setEdgesForExtendedLayout:UIRectEdgeNone];

    [self setPageViewController:
              [[UIPageViewController alloc]
                  initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl
                    navigationOrientation:
                        UIPageViewControllerNavigationOrientationHorizontal
                                  options:nil]];

    [[self pageViewController] setDataSource:self];
    [[self pageViewController] setDelegate:self];

    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Get current baby
    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];

    //Get current page
    _currentIndex = baby.babyCurrentPage;

    //Set pager and go to current page
    CGRect frame = [[self view] frame];
    [[[self pageViewController] view] setFrame:frame];
    [self.pageViewController
        setViewControllers:@[ [self viewControllerForIndex:_currentIndex] ]
                 direction:UIPageViewControllerNavigationDirectionReverse
                  animated:NO
                completion:nil];

    //Create index button
    if (![self.view.subviews containsObject:buttonIndex])
    {
        buttonIndex = [[UIButton alloc] initWithFrame:CGRectMake(1024 - 80, 30, 60, 60)];
        [buttonIndex addTarget:self action:@selector(menu:) forControlEvents:UIControlEventTouchUpInside];
        [buttonIndex setImage:[UIImage imageWithData:baby.babyPhoto] forState:UIControlStateNormal];
        [buttonIndex setBackgroundColor:kColor_Yellow];
        [buttonIndex setTitleColor:kColor_DarkText forState:UIControlStateNormal];
        buttonIndex.layer.cornerRadius = buttonIndex.frame.size.width / 2;
        buttonIndex.layer.borderColor = kColor_DarkOverlay.CGColor;
        buttonIndex.layer.borderWidth = 2.0f;
        buttonIndex.layer.masksToBounds = YES;
        [self.view addSubview:buttonIndex];
        [self.view bringSubviewToFront:buttonIndex];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


#pragma mark - System
#pragma mark -


/**
 *  Set status bar color
 *
 */
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}


/**
 *  Hides status bar in iOS7
 *
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}


/**
 *  Define allowed orientations
 *
 */
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


/**
 *  Enable/Disable autorotation
 *
 */
- (BOOL)shouldAutorotate
{
    return YES;
}


#pragma mark - End
#pragma mark -


- (void)viewWillDisappear:(BOOL)animated
{
    //Close keyboard if open
    [self.view endEditing:YES];

    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions
#pragma mark -


/**
 *  Swap menu position
 */
- (IBAction)menu:(id)sender
{
    if (menu.expanded == YES) //Already open
    {
        [self closeMenu];
    }
    else //Menu is closed
    {
        if (![self.view.subviews containsObject:menu.view])
        {
            menu = [storyboard instantiateViewControllerWithIdentifier:@"Menu"];
            [self addChildViewController:menu];
            [menu didMoveToParentViewController:self];

            [self.view addSubview:menu.view];
        }

        menu.view.frame = CGRectMake(0, -300, 1024, 300);

        [self openMenu];
    }
}


- (IBAction)babySelection:(id)sender
{
    [self closeMenu];

    //Flag to avoid skipping babyselection on initialization
    alreadyInitialized = YES;

    //Save current page for selected baby before switching
    NSString *bb = [defaults objectForKey:@"currentBaby"];
    BabyModel *baby = [BabyModel objectForPrimaryKey:bb];

    [defaultRealm beginWriteTransaction];
    baby.babyCurrentPage = self.currentIndex;
    [defaultRealm commitWriteTransaction];

    //Go to book
    BabySelector *selector = (BabySelector *)[storyboard instantiateViewControllerWithIdentifier:@"BabySelector"];

    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [delegate changeRootViewController:selector];
}


#pragma mark - Methods
#pragma mark -


/**
 *  Control menu drag behavior
 */
- (void)handleMenuPan:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIView *piece = [gestureRecognizer view];

    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
        CGFloat translateY = [piece center].y + translation.y;

        //Limits
        if (translateY > 100)
        {
            translateY = 100;
        }

        if (translateY < -140)
        {
            translateY = -140;
        }

        [piece setCenter:CGPointMake([piece center].x, translateY)];
        [gestureRecognizer setTranslation:CGPointZero inView:[piece superview]];
    }
    else if ([gestureRecognizer state] == UIGestureRecognizerStateEnded)
    {
        if ([piece center].y > -20) //Not enough. Open again
        {
            [self openMenu];
        }
        else //Close
        {
            [self closeMenu];
        }
    }
}


- (void)openMenu
{
    [UIView animateWithDuration:0.5f
        delay:0.0f
        usingSpringWithDamping:0.6f
        initialSpringVelocity:0.1f
        options:UIViewAnimationOptionCurveEaseOut
        animations:
            ^{
              menu.view.frame = CGRectMake(0, -30, 1024, 300);
            }
        completion:^(BOOL finished) {
          menu.expanded = YES;

          if (![menu.view.gestureRecognizers containsObject:pan])
          {
              pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMenuPan:)];
              [menu.view addGestureRecognizer:pan];
          }

        }];
}

- (void)closeMenu
{
    [UIView animateWithDuration:0.5f
        delay:0.0f
        usingSpringWithDamping:0.6f
        initialSpringVelocity:0.1f
        options:UIViewAnimationOptionCurveEaseIn
        animations:
            ^{
              menu.view.frame = CGRectMake(0, -300, 1024, 300);
            }
        completion:^(BOOL finished) {
          if ([menu.view.gestureRecognizers containsObject:pan])
          {
              [menu.view removeGestureRecognizer:pan];
          }

          [menu.view removeFromSuperview];
          [menu removeFromParentViewController];

          menu = nil;
        }];
}


- (void)setPage:(int)index animated:(BOOL)animated
{
    if (index != [self currentIndex])
    {
        [[self pageViewController]
            setViewControllers:@[ [self viewControllerForIndex:index] ]
                     direction:(index > _currentIndex) ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse
                      animated:animated
                    completion:nil];

        //Update index
        _currentIndex = index;
    }
}


- (UIViewController *)viewControllerForIndex:(int)index
{
#ifdef __x86_64__
    NSString *pageName = [NSString stringWithFormat:@"Page%02ld", index];
#else
    NSString *pageName = [NSString stringWithFormat:@"Page%02d", index];
#endif

    return [storyboard instantiateViewControllerWithIdentifier:pageName];
}


#pragma mark - UIPageViewControllerDelegate
#pragma mark -


- (UIViewController *)pageViewController:
                          (UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController
{
    if (_currentIndex > 0 && !transitioning)
    {
        isForward = NO;
        transitioning = YES;

        return [self viewControllerForIndex:_currentIndex - 1];
    }

    return nil;
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController
{
    if (_currentIndex < kNumberOfPages - 1 && !transitioning)
    {
        isForward = YES;
        transitioning = YES;

        return [self viewControllerForIndex:_currentIndex + 1];
    }

    return nil;
}


- (void)pageViewController:(UIPageViewController *)pageViewController
    willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
}


- (void)pageViewController:(UIPageViewController *)pageViewController
        didFinishAnimating:(BOOL)finished
   previousViewControllers:(NSArray *)previousViewControllers
       transitionCompleted:(BOOL)completed
{
    if (finished)
    {
        transitioning = NO;
    }

    if (completed)
    {
        transitioning = NO;

        if (isForward)
        {
            _currentIndex++;
        }
        else
        {
            _currentIndex--;
        }

        [menu updateCollection];
    }
}


@end
