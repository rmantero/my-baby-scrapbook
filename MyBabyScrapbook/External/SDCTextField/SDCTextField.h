//
//  SDCTextField.h
//  SDCTextField
//
//  Created by Scott Berrevoets on 6/18/13.
//  Copyright (c) 2013 Scotty Doesn't Code. All rights reserved.
//


#import <UIKit/UIKit.h>


/*! SDCTextField is a drop-in replacement for UITextField that allows the user to limit the maximum number of characters that can be entered in the text field.
 */
@interface SDCTextField : UITextField


/*! Defines the maximum number of characters that can be entered in the text field. The default is 0, which translates to a virtually unlimited number of characters. */
@property (nonatomic) NSUInteger maxLength;
//@property (nonatomic, assign) BOOL onlyNumbers; //FIXME:Added by MyScrapBook


@end
