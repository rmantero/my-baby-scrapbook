/*
 HFImageEditorViewController+Private.h
 MyBabyScrapbook

 Created by Ricardo Mantero on 8/3/15.
 Copyright (c) 2015 Dynambi. All rights reserved.
 */


#import "HFImageEditorViewController.h"


@interface HFImageEditorViewController (Private)


@property (nonatomic, retain) IBOutlet UIView<HFImageEditorFrame> *frameView;


- (void)startTransformHook;
- (void)endTransformHook;


@end
