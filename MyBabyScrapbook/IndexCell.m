//
//  IndexCell.m
//  MyBabyScrapbook
//
//  Created by Ricardo Mantero on 21/3/15.
//  Copyright (c) 2015 Dynambi. All rights reserved.
//


#import "IndexCell.h"


@implementation IndexCell


- (void)awakeFromNib
{
}


- (void)prepareForReuse
{
    [super prepareForReuse];
    _labelName.text = @"";
}


- (void)setSelected:(BOOL)selected
{
    if (selected)
    {
        self.labelName.textColor = kColor_Red;
    }
    else
    {
        self.labelName.textColor = kColor_DarkText;
    }
}


@end
