//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//


#import "___FILEBASENAME___.h"
#import "Globals.h"


@interface ___FILEBASENAMEASIDENTIFIER___ ()


@property (weak, nonatomic) IBOutlet THNotesTextView *textview1;
@property (weak, nonatomic) IBOutlet UIButton *buttonImage1;
@property (weak, nonatomic) IBOutlet SDCTextField *textfield1;


@end


#pragma mark - Initialization
#pragma mark -


@implementation ___FILEBASENAMEASIDENTIFIER___


- (void)viewDidLoad
{
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Pass current class to superclass
    self.pageEdited = self;

#pragma mark - EDIT FROM HERE
#pragma mark -

    //Exclusion textview1
    //TODO:Paste from Paintcode
//  _textview1.textContainer.exclusionPaths = @[exclusion1Path];

    //Clip textview1
    //TODO:Paste from Paintcode
//    [[UIColor clearColor] setFill];
//    [clip1Path fill];
//    _textview1.visibleArea = clip1Path;

    //Textviews
//    _textview1.drawWithoutFrame = YES;

    //Textfield
//    _textfield1.maxLength = 2;
//    _textfield1.inputView = [LNNumberpad defaultLNNumberpad];

    //Load page assets from Realm
    [self loadItem:_textview1];
    [self loadItem:_textfield1];
    [self loadItem:_buttonImage1];
}


/**
 *  Needed to allow this textfield resize dynamically as typing
 *
 */
//- (IBAction) textFieldDidChange: (UITextField*) textField
//{
//    [textField invalidateIntrinsicContentSize];
//}


@end
