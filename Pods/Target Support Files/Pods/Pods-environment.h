
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// HexColors
#define COCOAPODS_POD_AVAILABLE_HexColors
#define COCOAPODS_VERSION_MAJOR_HexColors 2
#define COCOAPODS_VERSION_MINOR_HexColors 2
#define COCOAPODS_VERSION_PATCH_HexColors 1

// IQKeyboardManager
#define COCOAPODS_POD_AVAILABLE_IQKeyboardManager
#define COCOAPODS_VERSION_MAJOR_IQKeyboardManager 3
#define COCOAPODS_VERSION_MINOR_IQKeyboardManager 2
#define COCOAPODS_VERSION_PATCH_IQKeyboardManager 3

// Realm
#define COCOAPODS_POD_AVAILABLE_Realm
#define COCOAPODS_VERSION_MAJOR_Realm 0
#define COCOAPODS_VERSION_MINOR_Realm 92
#define COCOAPODS_VERSION_PATCH_Realm 1

// Realm/Headers
#define COCOAPODS_POD_AVAILABLE_Realm_Headers
#define COCOAPODS_VERSION_MAJOR_Realm_Headers 0
#define COCOAPODS_VERSION_MINOR_Realm_Headers 92
#define COCOAPODS_VERSION_PATCH_Realm_Headers 1

